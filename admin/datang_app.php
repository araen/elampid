<?php
error_reporting(0);
require_once("header.php");

$on = '<img hspace="2" width="16" height="16" src="assets/images/s_okay.png" alt="Tercetak" title="Approval" border="0">';
$off = '<img hspace="2" width="16" height="16" src="assets/images/s_nokay.png" alt="Belum" title="Belum" border="0">';

$arr = array('NIK_PEMOHON','NAMA_PEMOHON','ALAMAT');
$limit = 15;

if($_GET[Page]=="") $page=1; else $page=$_GET[Page]; 
$top = $page * $limit;
$bottom = ($page-1) * $limit;

$Prev_Page = $page-1;  
$Next_Page = $page+1;  

$ID_KEC = $_SESSION["no_kec"];
$ID_KEL = $_SESSION["no_kel"];
$id_user = $_SESSION["id_user"];
$group = $_SESSION["group"];

if ($group == 'KEC') {
	$qry = $qry." and (flag_status = 3)"; 
}elseif ($group == 'KEL') {
	$qry = $qry." and (flag_status = 2)"; 
}elseif ($group == 'SPV') {
	$qry = $qry." and (flag_status = 4)"; 
}else{
	$qry = $qry." and (flag_status != 1 and flag_status != 2 and flag_status != 3)";
}

if($ID_KEC <> ''){
	$qry = $qry." and NK_KEC = ".$ID_KEC;
}

if($ID_KEL <> ''){
	$qry = $qry." and NK_KEL = ".$ID_KEL;
}

if ($group == 'KEC') { $flag_app=2;
}elseif ($group == 'KEL') { $flag_app=1;
}elseif ($group == 'SPV '){ $flag_app=3;
}else{ $flag_app=0;
}

if($_POST['Search']){
	$f=$_POST['mnuCari'];
	$t=trim(strtoupper($_POST['txtCari']));
	if($t<>'') { $qry=$qry."and $f = '$t'"; }
}
$q = ociparse($conn, "select count(*) as num_rows from DATANG_HEADER where id is not null ".$qry);
//echo $qry;
ociexecute($q);
if(ocifetch($q)){ $brs=OCIResult($q, "NUM_ROWS"); 
}
$num = ceil($brs/$limit);

$s="select * from (select a.*, rownum as row_number
from DATANG_HEADER a where rownum <= $top ".$qry." ) where row_number > $bottom ";
//echo $s;
$sql = ociparse($conn, $s);
ociexecute($sql); 
?>
<link href="style.css" rel="stylesheet" type="text/css" media="all" />
<script language="javascript">
function fOpen(url) {
	window.open(url, "_parent")
	window.focus();
}
function fDelete(id,nama){
	if (confirm("Apakah Data '"+nama+"' akan dihapus?")) { 
	}else{ return false };
	window.open("datang_exc.php?proc=delete&id="+id, "_parent")
}
function fApprove(id,nama,flag){
	if (confirm("Apakah Data '"+nama+"' akan di approval?")) { 
	}else{ return false };
	window.open("datang_exc.php?proc=app&id="+id+"&flag_app="+flag, "_parent")
}
function fApprove_btl(id,nama,flag){
	if (confirm("Apakah Data '"+nama+"' akan di batal approval?")) { 
	}else{ return false };
	window.open("datang_exc.php?proc=app_btl&id="+id+"&flag_app="+flag, "_parent")

}

</script>
<link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
<style>
.head1{
font-family:Nyala; font-size:36px; color:#121212; line-height:30px;
}
.head2{
font-family:Nyala; font-size:24px; color:#121212; line-height:30px;
}
.head3{
font-family:Nyala; font-size:20px; color:#121212; line-height:20px; margin-bottom:5px;
}
</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr class="tableheader">
        <td>&nbsp; pendaftaran pindah yang sudah di approval</td>
    </tr>
</table>
<fieldset>
<table width="100%">
	<tr class="subheader">
	<td width="5%" rowspan="2">NO</td>
	<td width="12%" rowspan="2">NIK PEMOHON</td>
	<td width="25%" rowspan="2">NAMA PEMOHON</td>
	<td width="20%" rowspan="2">ALAMAT</td>
	<td width="8%" rowspan="2">TGL APP</td>
	<td width="15%" colspan="3" align="center">STATUS</td>
	<td width="10%" rowspan="2">AKSI</td>
	</tr>
	<tr class="subheader">
	  <td align="center" width="5%">Kel</td>
      <td align="center" width="5%">Kec</td>
      <td align="center" width="5%">Dispenduk</td>
      <!--<td align="center" width="5%">SIAK</td> -->
  </tr>
	<?php
	$i=0;
	while(ocifetch($sql)) { $i++;
		if($i%2==0) { $class='tablecontent';
		}else { $class='tablecontent-odd'; } 
		$id=OCIResult($sql, "ID");
		$nama=OCIResult($sql, "NAMA_PEMOHON");
		$flag_status = OCIResult($sql, "FLAG_STATUS"); 
		/*$skpwni = OCIResult($sql, "SKPWNI"); */?>
		<tr class="<?php echo $class; ?>">
		<td><?php echo OCIResult($sql, "ROW_NUMBER"); ?></td>
		<td><?php echo OCIResult($sql, "NIK_PEMOHON"); ?></td>
		<td><?php echo OCIResult($sql, "NAMA_PEMOHON"); ?></td>
		<td><?php echo OCIResult($sql, "NK_ALAMAT"); ?></td>
			<td><?php echo date('d-m-Y',strtotime(OCIResult($sql, "MODIFIED_DATE"))); ?></td>
        <td align="center"><?php  if($flag_status > 1) { echo $on;}else{ echo $off;}?></td>
		<td align="center"><?php  if($flag_status > 2) { echo $on;}else{ echo $off;}?></td>
		<td align="center"><?php  if($flag_status > 3) { echo $on;}else{ echo $off;}?></td>
		<td>
			<img src="assets/images/b_edit.png" title="edit" class="img-button" onClick="fOpen('datang.php?id=<?php echo $id; ?>')" >
			<img src="assets/images/b_drop.png" title="hapus" class="img-button" onClick="fDelete('<?php echo $id; ?>','<?php echo $nama; ?>')">
			<img src="assets/images/btn_hitam.png" title="batal approve" class="img-button" onClick="fApprove_btl('<?php echo $id; ?>','<?php echo $nama; ?>',<?php echo ($flag_status - 1); ?>)">
        </td>
		</tr>
	<?php } ?>
</table>
<table width="100%">
	<tr class="button"> 
		<td>&nbsp;<img src="assets/images/b_add.gif" title="tambah data" class="img-button" onClick="fOpen('datang.php')" ></td>
	</tr>
</table>

<form name="frm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<table width="400" border="0" align="center" >
  <tr>
    <td width="30%" align="right">
		<select name="mnuCari" class="inputField" id="mnuCari">
			<?php for($i=0,$n=2;$i<=$n;$i++){ ?>
			<option class="inputField" value="<?php echo $arr[$i]; ?>" <?php if($_POST['mnuCari']==$arr[$i]) echo "selected";?>><?php echo $arr[$i] ;?></option>
			<?php } ?>
		</select>	
	</td>
    <td width="38%"><label>
      <input name="txtCari" type="text" id="txtCari" size="30" maxlength="30" value="<?php echo $_POST['txtCari']; ?>" />
    </label></td>
    <td width="32%"><label>
      <input name="Search" type="submit" id="Search" value="Search" />
    </label></td>
  </tr>
</table>
</form>
</fieldset>
<?php
$per_page = 1; // Number of items to show per page
$showeachside =5; //  Number of items to show either side of selected page
if(empty($page))$page=0;  // Current start position
$max_pages = ceil($brs / $limit); // Number of pages
$cur = ceil($page / $per_page) // Current page number
?>
<style type="text/css">
<!--
.pageselected {
    color: #FF0000;
    font-weight: bold;
}
-->
   </style>
<table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="PHPBODY">
<tr> 
<td width="99" align="center" valign="middle" bgcolor="#EAEAEA"> 
<?php
if(($page-$per_page) >= 1){
    $next = $page-$per_page;
?>
<a href ="<?=$_SERVER[SCRIPT_NAME]?>?Page=<?=$Prev_Page;?>">Prev</a>
<?php } ?>
</td>
<td width="201" align="center" valign="middle" class="selected">
Page <?=$cur;?> of  <?=$max_pages;?><br>
( <?=$brs;?> records )</td>
<td width="100" align="center" valign="middle" bgcolor="#EAEAEA"> 
<?php if($page<$num) { ?>
<a href ="<?=$_SERVER[SCRIPT_NAME]?>?Page=<?=$Next_Page;?>">Next</a>
<?php } ?>
</td>
</tr>
<tr> 
<td colspan="3" align="center" valign="middle" class="selected" style="padding-top:3px;"> 
<?php 
$eitherside = ($showeachside * $per_page);
if($page+1 > $eitherside)print (" .... ");
$pg=1;
for($y=0;$y<$num;$y+=$per_page)
{
    $class=($y==$page)?"pageselected":"";
    if(($y > ($page - $eitherside)) && ($y < ($page + $eitherside)))
    {
?>
	<a href ="<?=$_SERVER[SCRIPT_NAME]?>?Page=<?=$pg;?>"><?=$pg?></a>
<?php 
	}
    $pg++;
}
if(($page+$eitherside)<$num)print (" .... ");
?>
</td>
</tr>
</table>

<?php
require_once("footer.php");
//oci_close($conn);  

?>  
