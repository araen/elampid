<?php
error_reporting(0);
require_once("header.php");

$ID_KEC = $_SESSION["no_kec"];
$id_user = $_SESSION["id_user"];
$group = $_SESSION["group"];

$qry = "";  

$table_name = "DATANG_MONEV_ANG";

$txt = "select * from all_tables where table_name='$table_name'";
$sql = ociparse($conn, $txt);
ociexecute($sql);
if(ocifetch($sql)) { 
}else{
	$sql = 'CREATE TABLE TMP.'.$table_name.'("NO_KEC" NUMBER(2) NULL, "NAMA_KEC" VARCHAR2(60 BYTE) NULL, "JUMLAH" NUMBER NULL, "JUMLAH_ANGGOTA" NUMBER NULL) LOGGING NOCOMPRESS NOCACHE';
	ociexecute(ociparse($conn, $sql));
}

ociexecute(ociparse($conn, "delete from $table_name"));
$txt = "select a.no_kec,a.nama_kec from setup_kec a  where a.no_prop||a.no_kab='3578' ".$qry." order by a.no_kec";
$sql = ociparse($conn2, $txt);
ociexecute($sql); 
while(ocifetch($sql)) {
	$NO_KEC=OCIResult($sql, "NO_KEC"); 
	$NAMA_KEC=OCIResult($sql, "NAMA_KEC"); 
	ociexecute(ociparse($conn, "insert into $table_name (no_kec,nama_kec) values ('$NO_KEC','$NAMA_KEC')"));
}

$txt = "select nk_kec, count(*) as jumlah from tmp.datang_header where flag_status >= 1 group by nk_kec order by nk_kec";
$sql = ociparse($conn, $txt);
ociexecute($sql); 
while(ocifetch($sql)) {
	$NO_KEC=OCIResult($sql, "NK_KEC"); 
	$JUMLAH=OCIResult($sql, "JUMLAH"); 
	ociexecute(ociparse($conn, "update $table_name set JUMLAH='$JUMLAH' WHERE NO_KEC='$NO_KEC'"));
}

$txt = "select a.nk_kec, count(*) as jumlah_anggota from tmp.datang_header a, tmp.datang_detail b 
where a.id=b.id_detail and flag_status >= 1 group by a.nk_kec order by a.nk_kec";
$sql = ociparse($conn, $txt);
ociexecute($sql); 
while(ocifetch($sql)) {
	$NO_KEC=OCIResult($sql, "NK_KEC"); 
	$JUMLAH_ANGGOTA=OCIResult($sql, "JUMLAH_ANGGOTA"); 
	ociexecute(ociparse($conn, "update $table_name set JUMLAH_ANGGOTA='$JUMLAH_ANGGOTA' WHERE NO_KEC='$NO_KEC'"));
}

$sql = ociparse($conn, "select * from $table_name order by no_kec");
ociexecute($sql);

?>

<script language="javascript">
function fOpen(url) {
	window.open(url, "_parent")
	window.focus();
}
</script>
<link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
<style>
.button {font-weight:bold;}
/*td a { color:#000; text-decoration:none; } */
.head1{
font-family:Nyala; font-size:36px; color:#121212; line-height:30px;
}
.head2{
font-family:Nyala; font-size:24px; color:#121212; line-height:30px;
}
.head3{
font-family:Nyala; font-size:20px; color:#121212; line-height:20px; margin-bottom:5px;
}
</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr class="tableheader">
        <td>&nbsp;Monitoring data Permohonan Pindah Datang</td>
    </tr>
</table>
<fieldset>
<table width="100%">
	<tr class="subheader">
	<td width="5%">NO</td>
	<td width="20%">KECAMATAN</td>
	<td width="15%">JUMLAH PEMOHON</td> 
	<td width="60%">JUMLAH ANGGOTA</td>
	</tr>
	<?php
	$i=0;
	$JUMLAH=0;
	while(ocifetch($sql)) { $i++;
		if($i%2==0) { $class='tablecontent';
		}else { $class='tablecontent-odd'; } 
		$JUMLAH = $JUMLAH+OCIResult($sql, "JUMLAH");
		$JUMLAH_ANGGOTA = $JUMLAH_ANGGOTA+OCIResult($sql, "JUMLAH_ANGGOTA");
		$l = base64_encode(OCIResult($sql, "NO_KEC")."CCC");
		
		?>
		<tr class="<?php echo $class; ?>">
		<td><?php echo $i; ?></td>
		<td><a href="./monev-data.php?txt=<?php echo $l; ?>" target="_parent"><?php echo '('.OCIResult($sql, "NO_KEC").') '.OCIResult($sql, "NAMA_KEC"); ?></a></td>
		<td><?php echo number_format(OCIResult($sql, "JUMLAH")); ?></td>
		<td><?php echo number_format(OCIResult($sql, "JUMLAH_ANGGOTA")); ?></td>
		</tr>
	<?php } ?>
</table>
<table width="100%">
	<tr class="button"> 
        <td width="5%">&nbsp;</td>
        <td width="20%">&nbsp;</td>
        <td width="15%"><?php echo number_format($JUMLAH); ?></td> 
        <td width="60%"><?php echo number_format($JUMLAH_ANGGOTA); ?></td> 

	</tr>
</table>
</fieldset>
<?php
require_once("footer.php");
//oci_close($conn);  
?>  
