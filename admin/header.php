<?php
require_once("check_session.php");
require_once("config.php");

?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Permohonan Pindah Datang</title>

<!--script language="javascript">
function Logout(){
	if(confirm('Are You Sure to LogOut?')) window.parent.document.location.href='logout.php';
	else return false;
}
</script-->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/jquery.gritter.css" rel="stylesheet">
<link href="../css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
<style>
.button {font-weight:bold;}
/*td a { color:#000; text-decoration:none; } */
.head1{
font-family:Nyala; font-size:36px; color:#121212; line-height:30px;
}
.head2{
font-family:Nyala; font-size:24px; color:#121212; line-height:30px;
}
.head3{
font-family:Nyala; font-size:20px; color:#121212; line-height:20px; margin-bottom:5px;
}
</style>
 
<!--[if lt IE 9]>
<script src="html5.js" type="text/javascript"></script>
<![endif]-->

</head>

<body>
<div id="container" class="hfeed"> 
	<div class="page-header"><!--<img src="../assets/images/Logo-Surabaya.png" height="120" align="left" style="margin-left:150px" /> -->
<br>			<h1 style="margin-bottom:5px; font-size:48px"><a href="" title="Pendaftaran Pencatatan Pindah Keluar" rel="home">
&nbsp;PERMOHONAN PINDAH DATANG WNI</a></h1>
			<span class="admin-meta" style="font-size:24px">&nbsp;&nbsp;&nbsp;DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL KOTA SURABAYA</span>
			<!--span class="site-description">&nbsp;&nbsp;Jl. Tunjungan 1-3 Surabaya</span-->
			<!--
			<span class="site-description">Jl. Manyar Kertoarjo No. 6 Surabaya</span>
			-->
		<div style="clear:both;"></div>
		
	</div><!-- end of #header -->

	<div class="navbar">
	<div class="navbar-inner">
	<div class="container"> 
	<div class="nav-collapse collapse navbar-responsive-collapse">
    <ul class="nav">
		<li><a href="./">HOME</a></li>
		<li><a href="./datang_list.php">PINDAH DATANG</a></li>
		<li><a href="./datang_app.php">APPROVAL</a></li>
        <li><a href="./cetak.php">CETAK</a></li>
        <li><a href="./monev_anggota.php">MONEV</a></li>
		<li><a href="./user-list.php">USER MANAJEMEN</a></li>
        <!--
        <li><a href="#">MONEV</a>
			<ul>
		        <li><a href="./monev.php">MONEV PER KECAMATAN</a></li>
		        <li><a href="./monev_anggota.php">MONEV PER ANGGOTA</a></li>
            </ul>
		</li>
        -->
		<!--?php
			if($_SESSION['user_pindah']=="ADMIN") { echo '<li><a href="./skpwni.php">SKPWNI</a></li>';}
		?>
		<li><a href="./user-list.php">USER MANAJEMEN</a></li-->
		<!--
        <li><a href="#">UTILITY</a>
			<ul>
				<li><a href="./user-list.php">USER MANAJEMEN</a></li>
				<li><a href="./kecamatan-list.php">SETUP KECAMATAN</a></li>
			</ul>
		</li>
		-->
		<li><a href="logout.php">LOGOUT</a></li>	
		<li style="float:right"><a href="#">USER : <?=$_SESSION['user_pindah']?></a></li>		
    </ul>
</div>	
</div>
</div>		
</div>
<div class="clear"></div>
<!--<div class="container"> -->
