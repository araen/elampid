function pValidasi(){
	if ((document.getElementById('txtNIK').value) == "") {
		alert ("NIK harus diisi");
		document.getElementById('txtNIK').focus();
		return false;
	}
	if ((document.getElementById('txtNama').value) == "") {
		alert ("Nama harus diisi");
		document.getElementById('txtNama').focus();
		return false;
	}

	if ((document.getElementById('mnuSex').value) == "") {
		alert ("Pilih jenis kelamin");
		document.getElementById('mnuSex').focus();
		return false;
	}

	if ((document.getElementById('txtTmpLahir').value) == "") {
		alert ("Tempat lahir harus diisi");
		document.getElementById('txtTmpLahir').focus();
		return false;
	}
	
	if ((document.getElementById('txtTglLahir').value) == "") {
		alert ("Tanggal lahir harus diisi");
		document.getElementById('txtTglLahir').focus();
		return false;
	}

	var t_lahir = document.getElementById('txtTglLahir').value
	var tgl = t_lahir.substr(0,2);
	var bln = t_lahir.substr(3,2);
	var st = t_lahir.substr(2,1);
	var dw = t_lahir.substr(5,1);

	if ((st=="-") || (st=="/")) {
	}else{ 
		alert('Format tanggal lahir salah');
		document.getElementById('txtTglLahir').focus();
		return false;
	}

	if ((dw=="-") || (dw=="/")) {
	}else{ 
		alert('Format tanggal lahir salah');
		document.getElementById('txtTglLahir').focus();
		return false;
	}
	var i_tgl = parseInt(tgl,10);
	var i_bln = parseInt(bln,10);

	if((i_tgl < 1) || (i_tgl > 31)) {
		alert('Format tanggal lahir (tgl) salah');
		document.getElementById('txtTglLahir').focus();
		return false;
	}

	if((i_bln < 1) || (i_bln > 12)) {
		alert('Format tanggal lahir (bln) salah');
		document.getElementById('txtTglLahir').focus();
		return false;
	}

	if ((document.getElementById('mnuDarah').value) == "") {
		alert ("Pilih golongan darah");
		document.getElementById('mnuDarah').focus();
		return false;
	}

	if ((document.getElementById('mnuKawin').value) == "") {
		alert ("Pilih status kawin");
		document.getElementById('mnuKawin').focus();
		return false;
	}

	if ((document.getElementById('mnuPendidikan').value) == "") {
		alert ("Pilih pendidikan terakhir");
		document.getElementById('mnuPendidikan').focus();
		return false;
	}

	if ((document.getElementById('mnuPekerjaan').value) == "") {
		alert ("Pilih pekerjaan");
		document.getElementById('mnuPekerjaan').focus();
		return false;
	}

	if ((document.getElementById('mnuPropinsi2').value) == "") {
		alert ("Pilih propinsi asal");
		document.getElementById('mnuPropinsi2').focus();
		return false;
	}

	if ((document.getElementById('txtIdKabAsal').value) == "") {
		alert ("Pilih kabupaten asal");
		document.getElementById('txtIdKabAsal').focus();
		return false;
	}

	if ((document.getElementById('txtIdKecAsal').value) == "") {
		alert ("Pilih kecamatan asal");
		document.getElementById('txtIdKecAsal').focus();
		return false;
	}

	if ((document.getElementById('txtIdKelAsal').value) == "") {
		alert ("Pilih kelurahan asal");
		document.getElementById('txtIdKelAsal').focus();
		return false;
	}

	if ((document.getElementById('txtAlamatAsal').value) == "") {
		alert ("Alamt asal harus diisi");
		document.getElementById('txtAlamatAsal').focus();
		return false;
	}

	if ((document.getElementById('txtRTAsal').value) == "") {
		alert ("RT asal harus diisi");
		document.getElementById('txtRTAsal').focus();
		return false;
	}

	if ((document.getElementById('txtRWAsal').value) == "") {
		alert ("RW asal harus diisi");
		document.getElementById('txtRWAsal').focus();
		return false;
	}

	
	var t_lahir_penjamin = document.getElementById('txtTglLahirPenjamin').value;
	if (t_lahir_penjamin != "") {
		var tgl = t_lahir_penjamin.substr(0,2);
		var bln = t_lahir_penjamin.substr(3,2);
		var st = t_lahir_penjamin.substr(2,1);
		var dw = t_lahir_penjamin.substr(5,1);

		if ((st=="-") || (st=="/")) {
		}else{ 
			alert('Format tanggal lahir penjamin salah');
			document.getElementById('txtTglLahirPenjamin').focus();
			return false;
		}
	
		if ((dw=="-") || (dw=="/")) {
		}else{ 
			alert('Format tanggal lahir penjamin salah');
			document.getElementById('txtTglLahirPenjamin').focus();
			return false;
		}
		var i_tgl = parseInt(tgl,10);
		var i_bln = parseInt(bln,10);
	
		if((i_tgl < 1) || (i_tgl > 31)) {
			alert('Format tanggal lahir penjamin (tgl) salah');
			document.getElementById('txtTglLahirPenjamin').focus();
			return false;
		}
	
		if((i_bln < 1) || (i_bln > 12)) {
			alert('Format tanggal lahir penjamin (bln) salah');
			document.getElementById('txtTglLahirPenjamin').focus();
			return false;
		}
	}
	
	if ((document.getElementById('txtIdKec').value) == "") {
		alert ("Pilih kecamatan");
		document.getElementById('txtIdKec').focus();
		return false;
	}

	if ((document.getElementById('txtIdKel').value) == "") {
		alert ("Pilih kelurahan");
		document.getElementById('txtIdKel').focus();
		return false;
	}

	if ((document.getElementById('txtAlamat').value) == "") {
		alert ("Alamt harus diisi");
		document.getElementById('txtAlamat').focus();
		return false;
	}

	if ((document.getElementById('txtRT').value) == "") {
		alert ("RT harus diisi");
		document.getElementById('txtRT').focus();
		return false;
	}

	if ((document.getElementById('txtRW').value) == "") {
		alert ("RW harus diisi");
		document.getElementById('txtRW').focus();
		return false;
	}
	
	if ((document.getElementById('captcha').value) == "") {
		alert ("Captcha harus diisi");
		document.getElementById('captcha').focus();
		return false;
	}else{
		if ((document.getElementById('captcha').value) != (document.getElementById('txtCaptcha').value)) {		
			alert ("Captcha tidak sama");
			document.getElementById('captcha').focus();
			return false;
		}
	}

}
