<?php
require_once("functions.php");
if (!isset($_SESSION["user_pindah"])) {
	header("Location: login.php");
	exit(0);
} else {
	if (!login_check()) {
		header("Location: logout.php");
		exit(0);
	}

}

?>