<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<div id="footer" class="modal-footer">
	
		<div align="left" >
			&copy; Copyright 2016 by Dinas Kependudukan dan Catatan Sipil Kota Surabaya
		</div>
		<!-- end of .copyright -->

		<div class="grid col-300 scroll-top"><a href="#scroll-top" title="scroll to top">&uarr;</a></div>
		
		<!-- end .powered -->


	</div><!-- end #footer -->

<script type='text/javascript' src='js/responsive-scripts.min.js?ver=1.2.6'></script>
<script type='text/javascript' src='js/jquery.placeholder.min.js?ver=2.0.7'></script>
</body>
</html>