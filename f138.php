<?php
error_reporting(0);
require_once("config.php");
require_once("global.php");
require_once ("fpdf/fpdf.php");

$id = $_GET[id];


define('FPDF_FONTPATH','font/');

class PDF extends FPDF
{
//Page header
function Header()
{
}

function Footer() {


}
}

//Instanciation of inherited class
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',10);
$pdf->SetFillColor(236,232,212);
$arrStPindah = array('','Numpang KK','Membuat KK Baru','Nomor KK Tetap');
$arrKla = array('','Pindah Antar Kelurahan','Pindah Antar Kecamatan','Pindah Antar Kabupaten','Pindah Antar Propinsi','Pindah Keluar Negeri');
$arrSHDK = array('','KEPALA KELUARGA','SUAMI','ISTRI','ANAK','MENANTU','CUCU','ORANG TUA','MERTUA','FAMILI LAIN','PEMBANTU','LAINNYA');
$arrsex = array('','LAKI LAKI','PEREMPUAN');
$sql = ociparse($conn, "select a.* from DATANG_HEADER a where ID = ".$id);
ociexecute($sql);
if (ocifetch($sql)) {

	$NO_FORM = OCIResult($sql, "NO_FORM"); 
	$NO_KK = OCIResult($sql, "NO_KK"); 
	$NIK_KEP_KEL = OCIResult($sql, "NK_NIK_KEP_KEL"); 
	$NAMA_KEP_KEL = OCIResult($sql, "NK_NAMA_KEP_KEL"); 
	$NIK_PEMOHON = OCIResult($sql, "NIK_PEMOHON"); 
	$NAMA_PEMOHON = OCIResult($sql, "NAMA_PEMOHON"); 
	$ALAMAT = OCIResult($sql, "SRC_DUSUN"); 
	$RT = OCIResult($sql, "SRC_RT"); 
	$RW = OCIResult($sql, "SRC_RW"); 
	$FROM_NO_PROP = OCIResult($sql, "SRC_PROP"); 
	$FROM_NO_KAB = OCIResult($sql, "SRC_KAB"); 
	$FROM_NO_KEC = OCIResult($sql, "SRC_KEC"); 
	$FROM_NO_KEL = OCIResult($sql, "SRC_KEL"); 
	$TELP = OCIResult($sql, "SRC_TELP"); 
	$KODE_POS = OCIResult($sql, "SRC_KODE_POS"); 
	//$ALASAN_PINDAH = OCIResult($sql, "ALASAN_PINDAH"); 
	
	//$JENIS_PINDAH = OCIResult($sql, "JENIS_PINDAH"); 
	//$KLASIFIKASI_PINDAH = OCIResult($sql, "KLASIFIKASI_PINDAH"); 
	$STATUS_YG_PINDAH = OCIResult($sql, "STATUS_PINDAH"); 
	//$STATUS_YG_TDK_PINDAH = OCIResult($sql, "STATUS_YG_TDK_PINDAH"); 
	
	$DEST_NO_PROP = OCIResult($sql, "NK_PROP"); 
	$DEST_NO_KAB = OCIResult($sql, "NK_KAB"); 
	$DEST_NO_KEC = OCIResult($sql, "NK_KEC"); 
	$DEST_NO_KEL = OCIResult($sql, "NK_KEL"); 
	$DEST_ALAMAT = OCIResult($sql, "NK_ALAMAT"); 
	$DEST_KODE_POS = OCIResult($sql, "NK_KODE_POS"); 
	$DEST_TELP = OCIResult($sql, "NK_TELP"); 
	$DEST_NO_RT = OCIResult($sql, "NK_RT"); 
	$DEST_NO_RW = OCIResult($sql, "NK_RW"); 
	$TANGGAL_DATANG = ociresult($sql, "TGL_DATANG");
	$TANGGAL_DATANG = date('d-m-Y', strtotime($TANGGAL_DATANG));
	

	$q = ociparse($conn2, "SELECT * FROM SETUP_KEC WHERE NO_PROP||NO_KAB='3578' AND NO_KEC='$DEST_NO_KEC'");
	ociexecute($q);
	if (ocifetch($q)) { $DEST_NAMA_KEC = OCIResult($q, "NAMA_KEC"); }
	
	$q = ociparse($conn2, "SELECT * FROM SETUP_KEL WHERE NO_PROP||NO_KAB='3578' AND NO_KEC='$DEST_NO_KEC' AND NO_KEL='$DEST_NO_KEL'");
	ociexecute($q);
	if (ocifetch($q)) { $DEST_NAMA_KEL = OCIResult($q, "NAMA_KEL"); }
	

	/*$q= ociparse($conn2, "select * from REF_SIAK_WNI where sect='105' and NO='$ALASAN_PINDAH'");
	ociexecute($q);
	if (ocifetch($q)) { $ALASAN_PINDAH = OCIResult($q, "DESCRIP"); }*/

	$q = ociparse($conn2, "SELECT * FROM SETUP_PROP WHERE NO_PROP='$FROM_NO_PROP'");
	ociexecute($q);
	if (ocifetch($q)) { $FROM_NAMA_PROP = OCIResult($q, "NAMA_PROP"); }

	$q = ociparse($conn2, "SELECT * FROM SETUP_KAB WHERE NO_PROP='$FROM_NO_PROP' AND NO_KAB='$FROM_NO_KAB'");
	ociexecute($q);
	if (ocifetch($q)) { $FROM_NAMA_KAB = OCIResult($q, "NAMA_KAB"); }

	$q = ociparse($conn2, "SELECT * FROM SETUP_KEC WHERE NO_PROP='$FROM_NO_PROP' AND NO_KAB='$FROM_NO_KAB' AND NO_KEC='$FROM_NO_KEC'");
	ociexecute($q);
	if (ocifetch($q)) { $FROM_NAMA_KEC = OCIResult($q, "NAMA_KEC"); }
	
	$q = ociparse($conn2, "SELECT * FROM SETUP_KEL WHERE NO_PROP='$FROM_NO_PROP' AND NO_KAB='$FROM_NO_KAB' AND NO_KEC='$FROM_NO_KEC' AND NO_KEL='$FROM_NO_KEL'");
	ociexecute($q);
	if (ocifetch($q)) { $FROM_NAMA_KEL = OCIResult($q, "NAMA_KEL"); }
	
	/*$q = ociparse($conn2, "select * from REF_SIAK_WNI where sect='107' and NO='$JENIS_PINDAH'");
	ociexecute($q);
	if (ocifetch($q)) { $JENIS_PINDAH = OCIResult($q, "DESCRIP"); }*/
	
	//$q = ociparse($conn, "update tmp_akta_perkawinan set flag='1' where id='$id'");
	//ociexecute($q);

}

$pdf->Cell(0,0,'F.1-38', 0, 0, 'R', false);	
$pdf->Ln(5);

$pdf->Cell(40,0,'PROVINSI', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NO_PROP.' - '.$FROM_NAMA_PROP, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(40,0,'KABUPATEN/KOTA', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NO_KAB.' - '.$FROM_NAMA_KAB, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(40,0,'KECAMATAN', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NO_KEC.' - '.$FROM_NAMA_KEC, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(40,0,'DESA/KELURAHAN', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NO_KEL.' - '.$FROM_NAMA_KEL, 0, 0, 'L', false);	


$pdf->Ln(10);
$pdf->SetFont('Times','B',16);
$pdf->Cell(0,0,'FORMULIR PERMOHONAN DATANG WNI', 0, 0, 'C', false);	
$pdf->Ln(6);
$pdf->SetFont('Times','B',14);
$pdf->Cell(0,0,'Antar Kabupaten/Kota atau  Antar Provinsi', 0, 0, 'C', false);	
$pdf->Ln(6);
$pdf->Cell(0,0,'No. '.$NO_FORM, 0, 0, 'C', false);	

$pdf->Ln(10);
$pdf->SetFont('Times','B',14);
$pdf->SetFillColor(236,232,212);
$pdf->Cell(0,5,'DATA DAERAH ASAL', 0, 0, 'L', true);	
$pdf->Ln(8);

$pdf->SetFont('Times','',12);
$pdf->Cell(5,0,'1.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Nomor Kartu Keluarga', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0, $NO_KK, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(5,0,'2.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Nama Kepala Keluarga', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NAMA_KEP_KEL, 0, 0, 'L', false);	
$pdf->Ln(5);

/*$pdf->Cell(5,0,'3.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Alamat', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(70,0,$ALAMAT, 0, 0, 'L', false);	
$pdf->Cell(10,0,'RT', 0, 0, 'L', false);	
$pdf->Cell(10,0,$RT, 0, 0, 'L', false);	
$pdf->Cell(10,0,'RW', 0, 0, 'L', false);	
$pdf->Cell(10,0,$RW, 0, 0, 'L', false);	
$pdf->Ln(5);*/

if($L_ALAMAT>40){
	$pdf->Cell(60,0,'', 0, 0, 'C', false);	
	$pdf->MultiCell(100,5,$ALAMAT,0,'L',false);
	$pdf->Ln(5);
	$pdf->Cell(5,-25,'3.', 0, 0, 'C', false);	
	$pdf->Cell(50,-25,'Alamat', 0, 0, 'L', false);	
	$pdf->Cell(5,-25,':', 0, 0, 'C', false);	
	$pdf->Cell(100,0,'', 0, 0, 'C', false);	
	$pdf->Cell(7,-25,'RT', 0, 0, 'L', false);	
	$pdf->Cell(7,-25,$RT.',', 0, 0, 'L', false);	
	$pdf->Cell(8,-25,'RW', 0, 0, 'L', false);	
	$pdf->Cell(7,-25,$RW, 0, 0, 'L', false);	
	$pdf->Ln(-5);

}elseif($L_ALAMAT>25){
	$pdf->Cell(5,0,'3.', 0, 0, 'C', false);	
	$pdf->Cell(50,0,'Alamat', 0, 0, 'L', false);	
	$pdf->Cell(5,0,':', 0, 0, 'C', false);	
	$pdf->Cell(100,0,$ALAMAT, 0, 0, 'L', false);	
	$pdf->Cell(15,0,'RT/RW', 0, 0, 'L', false);	
	$pdf->Cell(15,0,' : '.$RT.'/'.$RW, 0, 0, 'L', false);	

}else{
	$pdf->Cell(5,0,'3.', 0, 0, 'C', false);	
	$pdf->Cell(50,0,'Alamat', 0, 0, 'L', false);	
	$pdf->Cell(5,0,':', 0, 0, 'C', false);	
	$pdf->Cell(70,0,$ALAMAT, 0, 0, 'L', false);	
	$pdf->Cell(15,0,'RT/RW', 0, 0, 'L', false);	
	$pdf->Cell(10,0,' : '.$RT.'/'.$RW, 0, 0, 'L', false);	
}

$pdf->Ln(5);



$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(45,0,'a. Desa/Kelurahan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(60,0,$FROM_NAMA_KEL, 0, 0, 'L', false);	
$pdf->Cell(25,0,'c. Kab/Kota', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_KAB, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(45,0,'b. Kecamatan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(60,0,$FROM_NAMA_KEC, 0, 0, 'L', false);	
$pdf->Cell(25,0,'d. Propinsi', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_PROP, 0, 0, 'L', false);	

$pdf->Ln(5);
$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(45,0,'e. Kode Pos', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(60,0,$KODE_POS, 0, 0, 'L', false);	
$pdf->Cell(25,0,'f. Telp', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$TELP, 0, 0, 'L', false);	

$pdf->Ln(5);
$pdf->Cell(5,0,'4.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'NIK Pemohon', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NIK_PEMOHON, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(5,0,'5.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Nama Lengkap', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NAMA_PEMOHON, 0, 0, 'L', false);	


$pdf->Ln(10);
$pdf->SetFont('Times','B',14);
$pdf->SetFillColor(236,232,212);
$pdf->Cell(0,5,'DATA DAERAH TUJUAN', 0, 0, 'L', true);	
$pdf->Ln(7);

$pdf->SetFont('Times','',12);
$pdf->Cell(5,0,'1.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Status KK', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$arrStPindah[$STATUS_YG_PINDAH], 0, 0, 'L', false);	
$pdf->Ln(5);
$pdf->Cell(5,0,'', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Bagi Yang Pindah', 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(5,0,'2.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Nomor Kartu Keluarga', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0, $NO_KK, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(5,0,'3.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'NIK Kepala Keluarga', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NIK_KEP_KEL, 0, 0, 'L', false);	
$pdf->Ln(5);


$pdf->Cell(5,0,'4.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Nama Kepala Keluarga', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NAMA_KEP_KEL, 0, 0, 'L', false);	
$pdf->Ln(5);

/*$pdf->Cell(5,0,'1.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Alamat Tujuan Pindah', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(70,0,$DEST_ALAMAT, 0, 0, 'L', false);	
$pdf->Cell(10,0,'RT', 0, 0, 'L', false);	
$pdf->Cell(10,0,$DEST_NO_RT, 0, 0, 'L', false);	
$pdf->Cell(10,0,'RW', 0, 0, 'L', false);	
$pdf->Cell(10,0,$DEST_NO_RW, 0, 0, 'L', false);	
$pdf->Ln(5);*/

$pdf->Cell(5,0,'5.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Tanggal Kedatangan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$TANGGAL_DATANG, 0, 0, 'L', false);	
$pdf->Ln(5);


if($L_ALAMAT_DEST>40){
	$pdf->Cell(60,0,'', 0, 0, 'C', false);	
	$pdf->MultiCell(100,5,$DEST_ALAMAT,0,'L',false);
	$pdf->Ln(5);
	$pdf->Cell(5,-25,'6.', 0, 0, 'C', false);	
	$pdf->Cell(50,-25,'Alamat Tujuan Pindah', 0, 0, 'L', false);	
	$pdf->Cell(5,-25,':', 0, 0, 'C', false);	
	$pdf->Cell(100,0,'', 0, 0, 'C', false);	
	$pdf->Cell(7,-25,'RT', 0, 0, 'L', false);	
	$pdf->Cell(7,-25,$DEST_NO_RT.',', 0, 0, 'L', false);	
	$pdf->Cell(8,-25,'RW', 0, 0, 'L', false);	
	$pdf->Cell(7,-25,$DEST_NO_RW, 0, 0, 'L', false);	
	$pdf->Ln(-5);
}elseif($L_ALAMAT_DEST>25){
	$pdf->Cell(5,0,'6.', 0, 0, 'C', false);	
	$pdf->Cell(50,0,'Alamat Tujuan Pindah', 0, 0, 'L', false);	
	$pdf->Cell(5,0,':', 0, 0, 'C', false);	
	$pdf->Cell(100,0,$DEST_ALAMAT, 0, 0, 'L', false);	
	$pdf->Cell(15,0,'RT/RW', 0, 0, 'L', false);	
	$pdf->Cell(10,0,' : '.$DEST_NO_RT.'/'.$DEST_NO_RW, 0, 0, 'L', false);	

}else{
	$pdf->Cell(5,0,'6.', 0, 0, 'C', false);	
	$pdf->Cell(50,0,'Alamat Tujuan Pindah', 0, 0, 'L', false);	
	$pdf->Cell(5,0,':', 0, 0, 'C', false);	
	$pdf->Cell(70,0,$DEST_ALAMAT, 0, 0, 'L', false);	
	$pdf->Cell(15,0,'RT/RW', 0, 0, 'L', false);	
	$pdf->Cell(10,0,' : '.$DEST_NO_RT.'/'.$DEST_NO_RW, 0, 0, 'L', false);	
}

$pdf->Ln(5);


$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(45,0,'a. Desa/Kelurahan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(60,0,$DEST_NAMA_KEL, 0, 0, 'L', false);	
$pdf->Cell(25,0,'c. Kab/Kota', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,'SURABAYA', 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(45,0,'b. Kecamatan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(60,0,$DEST_NAMA_KEC, 0, 0, 'L', false);	
$pdf->Cell(25,0,'d. Propinsi', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,'JAWA TIMUR', 0, 0, 'L', false);	

$pdf->Ln(5);
$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(45,0,'e. Kode Pos', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(60,0,$DEST_KODE_POS, 0, 0, 'L', false);	
$pdf->Cell(25,0,'f. Telp', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$DEST_TELP, 0, 0, 'L', false);	
$pdf->Ln(5);



/*$pdf->Cell(5,0,'3.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Jenis Kepindahan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$JENIS_PINDAH, 0, 0, 'L', false);	
$pdf->Ln(5);*/

/*$pdf->Cell(5,0,'2.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Status KK', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$arrStPindah[$STATUS_YG_TDK_PINDAH], 0, 0, 'L', false);	
$pdf->Ln(5);
/*$pdf->Cell(5,0,'', 0, 0, 'C', false);	
$pdf->Cell(70,0,'Bagi Yang Tidak Pindah', 0, 0, 'L', false);	
$pdf->Ln(5);*/



$pdf->Cell(5,0,'7.', 0, 0, 'C', false);	
$pdf->Cell(50,0,'Keluarga Yang Datang', 0, 0, 'L', false);	
$pdf->Ln(7);

$pdf->SetFont('Times','',10);

$header = array(
	array("label"=>"NO", "length"=>10, "align"=>"C"),
	array("label"=>"NIK", "length"=>30, "align"=>"L"),
	array("label"=>"NAMA LENGKAP", "length"=>75, "align"=>"L"),
	array("label"=>"TGL BERLAKU KTP", "length"=>35, "align"=>"L"),
	array("label"=>"SHDK", "length"=>40, "align"=>"L"),
);

$pdf->SetFillColor(236,232,212);
foreach ($header as $kolom) {
	$pdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
}	
$pdf->Ln(5);
$sql_dtl = ociparse($conn, "select a.*
        from DATANG_DETAIL a where ID_DATANG = ".$id);
ociexecute($sql_dtl);
while(ocifetch($sql_dtl)){$i++;
	$pdf->Cell(10,5,$i, 1, '0', 'C', false);	
	$pdf->Cell(30,5,OCIResult($sql_dtl,'NIK'), 1, '0', 'L', false);		
	$pdf->Cell(75,5,OCIResult($sql_dtl,'NAMA_LENGKAP'), 1, '0', 'L', false);		
	$pdf->Cell(35,5,OCIResult($sql_dtl,'TGL_KTP'), 1, '0', 'L', false);		
	$pdf->Cell(40,5,$arrSHDK[OCIResult($sql_dtl,'SHDK')], 1, '0', 'L', false);		
	$pdf->Ln(5);

}

$x = $pdf->GetX();
$y = $pdf->GetY();
$col1="Surat Keterangan Pindah (SKP) ini berlaku sebagai pengganti KTP selama KTP baru belum diterbitkan, paling lama 30 hari kerja sejak SKP diterbitkan.";
$pdf->MultiCell(0, 5, $col1, 0, 1);

$pdf->Ln(15);

$pdf->Cell(60,0,'', 0, 0, 'C', false);	
$pdf->Cell(71,0,'Mengetahui,', 0, 0, 'C', false);	
$pdf->Cell(60,0,'Surabaya, '.ShowDate(date('y-m-d'),5), 0, 0, 'C', false);	
$pdf->Ln(5);
$pdf->Cell(20,0,'', 0, 0, 'C', false);	
$pdf->Cell(20,0,'Petugas Registrasi', 0, 0, 'C', false);
$pdf->Cell(20,0,'', 0, 0, 'C', false);	
$pdf->Cell(70,0,'Camat', 0, 0, 'C', false);	
$pdf->Cell(5,0,'', 0, 0, 'C', false);	
$pdf->Cell(60,0,'Pemohon', 0, 0, 'C', false);	
$pdf->Ln(15);
$pdf->Cell(20,0,'', 0, 0, 'C', false);	
$pdf->Cell(20,0,'_________________________', 0, 0, 'C', false);	
$pdf->Cell(20,0,'', 0, 0, 'C', false);	
$pdf->Cell(70,0,'_________________________', 0, 0, 'C', false);	
$pdf->Cell(5,0,'', 0, 0, 'C', false);	
$pdf->Cell(60,0,'_________________________', 0, 0, 'C', false);	
$pdf->Ln(10);


$pdf->Cell(0,0,'Keterangan :', 0, 0, 'L', false);	
$pdf->Ln(7);
$pdf->Cell(0,0,'*) Diisi Oleh Petugas', 0, 0, 'L', false);	
$pdf->Ln(5);
$pdf->Cell(0,0,'- Formulir ini diisi di Desa/Kelurahan.', 0, 0, 'L', false);	
$pdf->Ln(5);
$pdf->Cell(0,0,'- Lembar  1 dibawa oleh pemohon dan diarsipkan di Kecamatan.', 0, 0, 'L', false);	
$pdf->Ln(5);
$pdf->Cell(0,0,'- Lembar  2 untuk pemohon.', 0, 0, 'L', false);	
$pdf->Ln(5);
$pdf->Cell(0,0,'- Lembar  3 diarsipkan di Desa/Kelurahan.', 0, 0, 'L', false);	
$pdf->Ln(5);


$pdf->Output();
?> 
