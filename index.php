<?php
include 'header.php';
?>

<div class="content">
	<div id="post" class="post">
		<h1>Selamat Datang,</h1>
		<div class="post-entry">
			<p style="font-size:18px">Terima kasih telah membuka website kami. Berikut kami informasikan tata cara Pendaftaran Pindah Datang : </p>
			<ul style="font-size:16px">
				<li style="line-height:28px">Scan dokumen persyaratan berikut ini sebelum mengisi form pendaftaran: </li>
					<ul>
					<li style="line-height:28px">Surat Keterangan Pindah WNI antar Kabupaten / Kota / Provinsi dari Dispenduk asal</li>
					<li style="line-height:28px">Akta Kawin</li> 
					<li style="line-height:28px">Surat Pernyataan mengenai keterangan Tempat Tinggal dari Kepala Keluarga yang diketahui oleh Ketua RT dan Ketua RW atau 
					Surat Pernyataan mengenai Jaminan Tempat Tinggal dari Kepala Keluarga yang akan ditumpangi, dan diketahui oleh Ketua RT dan Ketua RW, apabila menumpang pada domisili penduduk tersebut (Dilampiri oleh KTP dan Kartu Keluarga yang akan ditumpangi)</li> 
					 <li style="line-height:28px">Surat Pernyataan Jaminan Pekerjaan</li>  
					</ul>
				<li style="line-height:28px">Mengisi form Pendaftaran dengan benar</li> 
				<li style="line-height:28px">Mencetak Tanda Bukti Pendaftaran</li> 
				<li style="line-height:28px">Membawa Hasil Cetak Form Pendaftaran beserta kelengkapan persyaratan ke Kecamatan untuk diverifikasi Petugas.</li>
			</ul>
		</div>
		<!-- end of .post-entry -->
	</div><!-- end of #post-16 -->
</div><!-- end of #content -->


<?php
include 'footer.php';
?>
