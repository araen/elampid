<?php
    session_start();

    // Fungsi buka encrypt
    function buka_encrypt($ID){
        return urldecode(base64_decode($ID));
    }
    // Fungsi tutup encrypt
    function tutup_encrypt($ID){
        return urlencode(base64_encode($ID));
    }
    
    function uploadFile($path = '', $file = array(), $prefix = '', $suffix = '') {
        
        $path = $path . date('Y') . '/' . $prefix .'/';

        $status = true;
        if(!empty($path) and !is_dir($path)) 
            $status = mkdir($path, 0777, true);

        if( $status ) 
        {
            $basename = explode('.',basename($file["name"]));
            $filename = $file["name"];
            
            // rename file
            $file["name"] = $prefix.'_'.$suffix.'.'.$basename[1];
            
            if ($file['error'] == UPLOAD_ERR_OK)
            {
                try 
                {
                    if ( ! move_uploaded_file( $file['tmp_name'] , $path.$file["name"]) ) 
                    {
                        throw new Exception('Could not move file ' . $filename . ' to '. $path);
                    }
                    return $file["name"];
                } 
                catch (Exception $e) {
                            die ('File did not upload: ' . $e->getMessage());
                }
            }
        }
    }
    
    function insertRecord($conn, $table = "", $record = array()) {
        if(isset($record)) 
        {
            foreach ( $record as $key => $val ) {
                $a_key[] = $key;   
                $a_val[] = (strpos($val, "{") === false ) ? "'$val'" : str_replace("{", "", str_replace("}", "", $val));  
            }
            
            $sql = "insert into $table (";
            $sql .= implode(',', $a_key) . ") values (".implode(',', $a_val) .")";

            oci_commit($conn);
            
            $stid = oci_parse($conn, $sql);
            $status = oci_execute($stid);

            $sql = ociparse($conn, "select max(ID) ID from $table");
            ociexecute($sql);
        
            while ($row = oci_fetch_array ($sql, OCI_ASSOC)) {
                $a_row = $row;        
            }
            
            return $a_row['ID'];
        }
    }
    
    function updateRecord($conn, $table = "", $record = array(), $id) {
        if(isset($record)) 
        {
            $sql = "update $table set ";
            
            foreach ( $record as $key => $val ) {
                $val = (strpos($val, "{") === false ) ? "'$val'" : str_replace("{", "", str_replace("}", "", $val));  
            
                $a_val[] = "$key = $val";
            }
            
            $sql.= implode(',',$a_val);
            $sql.= " where ID = $id";

            $stid = oci_parse($conn, $sql);
            $status = oci_execute($stid);

            return $id;
        }
    }
    
    function deleteRecord($conn, $table = "", $key = "ID", $id) {
        $sql = "delete from $table where $key = $id";

        $stid = oci_parse($conn, $sql);
        $status = oci_execute($stid);

        return $id;
    }
    
    function genNoDatang($conn, $date) {
        $sql = "select max(no_datang) no_datang from datang_detail where no_datang like 'SKDWNI/3578/$date/%'";
        $stid = oci_parse($conn, $sql);
        oci_execute($stid);
        
        $no_datang = 0;
        while ($row = oci_fetch_array ($stid, OCI_ASSOC)) {
            $no_datang = $row['NO_DATANG'];
        }

        $no_datang = explode("/", $no_datang);
        $maxno_datang = str_pad((int) $no_datang[3] + 1, 4, '0', STR_PAD_LEFT);
        
        return "SKDWNI/3578/$date/$maxno_datang";
    }
    
    function genNoForm($conn, $date) {
        $sql = "select max(no_form) no_form from datang_header where no_form like 'REG/3578/$date/%'";
        $stid = oci_parse($conn, $sql);
        oci_execute($stid);
        
        $no_datang = 0;
        while ($row = oci_fetch_array ($stid, OCI_ASSOC)) {
            $no_datang = $row['NO_FORM'];
        }

        $no_datang = explode("/", $no_datang);
        $maxno_datang = str_pad((int) $no_datang[3] + 1, 4, '0', STR_PAD_LEFT);
        
        return "REG/3578/$date/$maxno_datang";
    }
    
    function debug($data, $isexit = true) {
        if(is_array($data)) {
            echo "<pre>";
            print_r($data);
            echo "</pre>";
        }
        elseif(is_object($data)) {
            echo "<pre>";
            var_dump($data);
            echo "</pre>";
        }else {
            echo $data;
        }
        
        if($isexit) {
            die();
        }
    }
    
    function thisPage() {
        return end(explode('/', $_SERVER['PHP_SELF']));
    }
    
    function setFlash($page, $message = array()) {
        $_SESSION[$page]['flash']['page'] = $page;    
        $_SESSION[$page]['flash']['status'] = $message[0];    
        $_SESSION[$page]['flash']['message'] = $message[1];    
    }
    
    function getFlash($page) {
        if(isset($_SESSION[$page]['flash']['message'])) { 
            $i_message = $_SESSION[$page]['flash'];
            
            unset($_SESSION[$page]);
            
            return $i_message;
        }
        else
            return null;
    }
?> 