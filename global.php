<?php
function ShowDate($str_date,$type=1){
	$T['Day']=array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
	$T['Month']=array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
	$T['S_Month']=array("","Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agst","Sep","Okt","Nov","Des");
	
	$longday=$T['Day'][date("w",strtotime($str_date))]; // NAMA HARI
	$day=date("d",strtotime($str_date)); // TGL
	$month=$T['Month'][date("n",strtotime($str_date))]; // NAMA BULAN
	$s_month=$T['S_Month'][date("n",strtotime($str_date))]; // NAMA BULAN
	$year=date("Y",strtotime($str_date)); // TAHUN
	$s_year = date("y",strtotime($str_date));
	$hour=date("H",strtotime($str_date)); // JAM
	$minute=date("i",strtotime($str_date)); // MENIT
	$second=date("s",strtotime($str_date)); // DETIK
	
	if ($type==1){
		return $longday.", ".$day." ".$month." ".$year."  ".$hour.":".$minute.":".$second;
	}else if ($type==2){
		return $longday.", ".$day." ".$month." ".$year." - ".$hour.":".$minute." ";
	}else if ($type==3){
		return $longday.", ".$day." ".$month." ".$year;
	}else if ($type==4){
		return $day." ".$month." ".$year." - ".$hour.":".$minute." ";
	}else if ($type==5){
		return $day." ".$month." ".$year;
	}else if ($type==6){
		return $day." ".$s_month." ".$year;
	}else if($type==7){
		return $longday." ".$hour.":".$minute;
	}else if($type==8){
		return $month." ".$year;
	}else if($type==9){
		return $longday.", ".$day." ".$s_month." ".$s_year." - ".$hour.":".$minute." ";
	}
}

function pReplace($obj) {
	$grs = '\ ';
	$syntax = str_replace(trim($grs),'/',$obj);
	$syntax = $obj;
	$syntax = str_replace("'",'`',$syntax);
	$syntax = str_replace('"','`',$syntax);
	$syntax = str_replace("\`",'`',$syntax);
	return $syntax;
}

function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . " belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

function Terbilang2($x)
{
  $abil2a = array("", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven","twelve","thirteen");
  $abil2b = array("", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eight", "ninety");
  if ($x < 14)
    return " " . $abil2a[$x];
  elseif ($x < 20)
    return Terbilang2($x - 10) . "teen";
  elseif ($x < 100)
    return $abil2b[substr($x,0,1)]. " " . Terbilang2($x % 10);
  elseif ($x < 1000)
    return Terbilang2($x / 100) . " hundred " . Terbilang2($x % 100);
  elseif ($x < 1000000)
    return Terbilang2($x / 1000) . " thousand " . Terbilang2($x % 1000);
}

function pMnuJKel($fld) {
	include 'config.php';
	$sqlSex = ociparse($conn2, "select * from REF_SIAK_WNI where sect='801' order by NO");
	ociexecute($sqlSex);
	$cmbSex="<select name='mnuSex' id='mnuSex' required='required'>";
	$cmbSex.="<option value=''>-- Pilih --</option>";
	while (ocifetch($sqlSex)) {
		$cmbSex.="<option value='".OCIResult($sqlSex, 'NO')."'";
		if(OCIResult($sqlSex, 'NO')==$fld) $cmbSex.=" selected";
		$cmbSex.=">".OCIResult($sqlSex, 'DESCRIP')."</option>"; 
	}
	$cmbSex.="</select>";
	return $cmbSex;
}

function pMnuGolDrh($fld) {
	include 'config.php';
	$sqlDarah = ociparse($conn2, "select * from REF_SIAK_WNI where sect='401' order by NO");
	ociexecute($sqlDarah);
	$cmbDarah="<select name='mnuDarah' id='mnuDarah' required='required'>";
	$cmbDarah.="<option value=''>-- Pilih Gol Darah --</option>";
	while (ocifetch($sqlDarah)) {
		$cmbDarah.="<option value='".OCIResult($sqlDarah, 'NO')."'";
		if(OCIResult($sqlDarah, 'NO')==$fld) $cmbDarah.=" selected";
		$cmbDarah.=">".OCIResult($sqlDarah, 'DESCRIP')."</option>"; 
	}
	$cmbDarah.="</select>";	
	return $cmbDarah;
}

function pMnuAgama($fld) {
	include 'config.php';
	$sqlAgama = ociparse($conn2, "select * from REF_SIAK_WNI where sect='501' order by NO");
	ociexecute($sqlAgama);
	$cmbAgama="<select name='mnuAgama' id='mnuAgama' required='required'>";
	$cmbAgama.="<option value=''>-- Pilih Agama --</option>";
	while (ocifetch($sqlAgama)) {
		$cmbAgama.="<option value='".OCIResult($sqlAgama, 'NO')."'";
		if(OCIResult($sqlAgama, 'NO')==$fld) $cmbAgama.=" selected";
		$cmbAgama.=">".OCIResult($sqlAgama, 'DESCRIP')."</option>"; 
	}
	$cmbAgama.="</select>";	
	return $cmbAgama;
}

function pMnuKawin($fld) {
	include 'config.php';
	$sqlKawin = ociparse($conn2, "select * from REF_SIAK_WNI where sect='601' order by NO");
	ociexecute($sqlKawin);
	$cmbKawin="<select name='mnuKawin' id='mnuKawin' required='required'>";
	$cmbKawin.="<option value=''>-- Pilih Status Kawin --</option>";
	while (ocifetch($sqlKawin)) {
		$cmbKawin.="<option value='".OCIResult($sqlKawin, 'NO')."'";
		if(OCIResult($sqlKawin, 'NO')==$fld) $cmbKawin.=" selected";
		$cmbKawin.=">".OCIResult($sqlKawin, 'DESCRIP')."</option>"; 
	}
	$cmbKawin.="</select>";	
	return $cmbKawin;
}

function pMnuPddk($fld) {
	include 'config.php';
	$sqlPendidikan = ociparse($conn2, "select * from REF_SIAK_WNI where sect='101' order by NO");
	ociexecute($sqlPendidikan);
	$cmbPendidikan="<select name='mnuPendidikan' id='mnuPendidikan' required='required'>";
	$cmbPendidikan.="<option value=''>-- Pilih Pendidikan --</option>";
	while (ocifetch($sqlPendidikan)) {
		$cmbPendidikan.="<option value='".OCIResult($sqlPendidikan, 'NO')."'";
		if(OCIResult($sqlPendidikan, 'NO')==$fld) $cmbPendidikan.=" selected";
		$cmbPendidikan.=">".OCIResult($sqlPendidikan, 'DESCRIP')."</option>"; 
	}
	$cmbPendidikan.="</select>";	
	return $cmbPendidikan;
}

function pMnuPkrjn($fld) {
	include 'config.php';
	$sqlPekerjaan = ociparse($conn2, "select * from PKRJN_MASTER where no not in (51,52,53,54,55,56,57,58,59,60,61,88) order by no");
	ociexecute($sqlPekerjaan);
	$cmbPekerjaan="<select name='mnuPekerjaan' id='mnuPekerjaan' required='required'>";
	$cmbPekerjaan.="<option value=''>-- Pilih Pekerjaan --</option>";
	while (ocifetch($sqlPekerjaan)) {
		$cmbPekerjaan.="<option value='".OCIResult($sqlPekerjaan, 'NO')."'";
		if(OCIResult($sqlPekerjaan, 'NO')==$fld) $cmbPekerjaan.=" selected";
		$cmbPekerjaan.=">".OCIResult($sqlPekerjaan, 'DESCRIP')."</option>"; 
	}
	$cmbPekerjaan.="</select>";	
	return $cmbPekerjaan;
}

function pMnuPkrjn2($fld) {
	include 'config.php';
	$sqlPekerjaan = ociparse($conn2, "select * from PKRJN_MASTER where no not in (51,52,53,54,55,56,57,58,59,60,61,88) order by no");
	ociexecute($sqlPekerjaan);
	$cmbPekerjaan="<select name='mnuPekerjaanPenjamin' id='mnuPekerjaanPenjamin' required='required'>";
	$cmbPekerjaan.="<option value=''>-- Pilih Pekerjaan --</option>";
	while (ocifetch($sqlPekerjaan)) {
		$cmbPekerjaan.="<option value='".OCIResult($sqlPekerjaan, 'NO')."'";
		if(OCIResult($sqlPekerjaan, 'NO')==$fld) $cmbPekerjaan.=" selected";
		$cmbPekerjaan.=">".OCIResult($sqlPekerjaan, 'DESCRIP')."</option>"; 
	}
	$cmbPekerjaan.="</select>";	
	return $cmbPekerjaan;
}


?>