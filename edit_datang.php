<?php
error_reporting(0);
require_once("header.php");
require_once("global.php");

if(isset($_GET['id'])) {$ID=(get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);}

$arr = array('NIK','NAMA_LGKP');
?>
<script language="javascript">
function fOpen(url) {
	window.open(url, "_parent")
	window.focus();
}
</script>
<style>
th, td, table { border:none; padding:1px 0;}
.subheader {background:#333333; color:#fff; font-weight:bold; }
.subheader td { border:1px solid #dddddd;  padding: 0.8em;}
.tablecontent td { border:1px solid #dddddd;  padding: 0.8em;}
</style>

<link href="css/styles.css" rel="stylesheet" type="text/css" />
<link href="assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/bootstrap/prettify.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet"> 

<form name="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
<div class="content">
<h1 style="margin-left:80px">Edit Permohonan Pindah</h1>

<fieldset style="margin-left:80px">
<legend class="legend-h"><b><i>Edit Permohonan Pindah</i></b></legend>
<form name="frm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<table border="0">
	<tr>
	  <td width="30%" align="right">NIK Pemohon &nbsp;</td>
	  <td width="70%"><input name="txtNIK" type="text" id="txtNIK" size="20" maxlength="16" value="<?php echo $_POST['txtNIK'];?>" required="required">&nbsp;<span class="error">&nbsp;*</span>&nbsp;</td>
    </tr>

	<tr>
	  <td align="right">Nama Lengkap &nbsp;</td>
	  <td><input name="txtNama" type="text" id="txtNama" size="50" maxlength="60" value="<?php echo $_POST['txtNama'];?>" required="required">
	  <span class="error">&nbsp;*</span></td>
	</tr>
	<tr class="button">
		<td align="right">&nbsp;</td>
		<td align="left"><input name="submit" value="Cari.." type="submit" /></td>
	</tr>

</table>
</form>
</fieldset>
</div>
<div class="content">
<fieldset style="margin-left:80px">
<?php
if($_POST['submit']) {

	$NIK = trim($_POST['txtNIK']);
	$NAMA_LGKP = trim(strtoupper($_POST['txtNama']));
	
	$sql = ociparse($conn, "select a.* from datang_header a where NIK_PEMOHON='$NIK' and NAMA_PEMOHON='$NAMA_LGKP'");
	
	ociexecute($sql);
	?>
<table width="100%" id="subtable" align="center">
	<tr class="subheader">
			<td width="3%">NO</td>
			<td width="20%">NIK</td>
			<td width="30%">NAMA LENGKAP</td>
			<td width="20%">ALAMAT ASAL</td>
			<td width="17%">EDIT</td>
		</tr>
		<?php 
		$i=0;
		while(ocifetch($sql)) { $i++;
			if($i%2==0) { $class='tablecontent';
			}else { $class='tablecontent-odd'; }
			$class='tablecontent';
			$id=base64_encode(base64_encode(base64_encode(OCIResult($sql,'ID'))));
			$id=OCIResult($sql,'ID');
			?>
		<tr class="<?php echo $class; ?>" >
			<td><?php echo $i; ?></td>
			<td><?php echo OCIResult($sql,'NIK_PEMOHON'); ?></td>
			<td><?php echo OCIResult($sql,'NAMA_PEMOHON'); ?></td>
			<td><?php echo OCIResult($sql,'SRC_DUSUN'); ?></td>
			<td><img src="images/btn.png" style="cursor:pointer" onclick="fOpen('datang.php?id=<?php echo $id; ?>')" ></td>
		</tr>
		<?php } ?>
	</table>
	
<?php
}
?>
</fieldset>
</div>
</form>

<?php
require_once("footer.php");
?>
