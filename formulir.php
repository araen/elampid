<?php
error_reporting(0);
require_once("config.php");
require_once("global.php");
require_once ("fpdf/fpdf.php");

$id = $_GET[id];


define('FPDF_FONTPATH','font/');

class PDF extends FPDF
{
//Page header
function Header()
{
}

function Footer() {


}
}

//Instanciation of inherited class
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',10);
$pdf->SetFillColor(236,232,212);
$arrStPindah = array('','Numpang KK','Membuat KK Baru','Nomor KK Tetap');
$arrKla = array('','Pindah Antar Kelurahan','Pindah Antar Kecamatan','Pindah Antar Kabupaten','Pindah Antar Propinsi','Pindah Keluar Negeri');
$arrSHDK = array('','KEPALA KELUARGA','SUAMI','ISTRI','ANAK','MENANTU','CUCU','ORANG TUA','MERTUA','FAMILI LAIN','PEMBANTU','LAINNYA');
$arrsex = array('','LAKI LAKI','PEREMPUAN');
$sql = ociparse($conn, "select a.* from DATANG_HEADER a where ID = ".$id);
ociexecute($sql);
if (ocifetch($sql)) {
	
	$NO_FORM = OCIResult($sql, "NO_FORM"); 
	$NO_KK = OCIResult($sql, "NO_KK"); 
	$NIK_KEP_KEL = OCIResult($sql, "NK_NIK_KEP_KEL"); 
	$NAMA_KEP_KEL = OCIResult($sql, "NK_NAMA_KEP_KEL"); 
	$NIK_PEMOHON = OCIResult($sql, "NIK_PEMOHON"); 
	$NAMA_PEMOHON = OCIResult($sql, "NAMA_PEMOHON"); 
	$ALAMAT = OCIResult($sql, "SRC_DUSUN"); 
	$RT = OCIResult($sql, "SRC_RT"); 
	$RW = OCIResult($sql, "SRC_RW"); 
	$FROM_NO_PROP = OCIResult($sql, "SRC_PROP"); 
	$FROM_NO_KAB = OCIResult($sql, "SRC_KAB"); 
	$FROM_NO_KEC = OCIResult($sql, "SRC_KEC"); 
	$FROM_NO_KEL = OCIResult($sql, "SRC_KEL"); 
	$TELP = OCIResult($sql, "SRC_TELP"); 
	$KODE_POS = OCIResult($sql, "SRC_KODE_POS"); 
	//$ALASAN_PINDAH = OCIResult($sql, "ALASAN_PINDAH"); 
	
	//$JENIS_PINDAH = OCIResult($sql, "JENIS_PINDAH"); 
	//$KLASIFIKASI_PINDAH = OCIResult($sql, "KLASIFIKASI_PINDAH"); 
	$STATUS_YG_PINDAH = OCIResult($sql, "STATUS_PINDAH"); 
	//$STATUS_YG_TDK_PINDAH = OCIResult($sql, "STATUS_YG_TDK_PINDAH"); 
	
	$DEST_NO_PROP = OCIResult($sql, "NK_PROP"); 
	$DEST_NO_KAB = OCIResult($sql, "NK_KAB"); 
	$DEST_NO_KEC = OCIResult($sql, "NK_KEC"); 
	$DEST_NO_KEL = OCIResult($sql, "NK_KEL"); 
	$DEST_ALAMAT = OCIResult($sql, "NK_ALAMAT"); 
	$DEST_KODE_POS = OCIResult($sql, "NK_KODE_POS"); 
	$DEST_TELP = OCIResult($sql, "NK_TELP"); 
	$DEST_NO_RT = OCIResult($sql, "NK_RT"); 
	$DEST_NO_RW = OCIResult($sql, "NK_RW"); 
	$TANGGAL_DATANG = ociresult($sql, "TGL_DATANG");
	$TANGGAL_DATANG = date('d-m-Y', strtotime($TANGGAL_DATANG));
	

	$q = ociparse($conn2, "SELECT * FROM SETUP_KEC WHERE NO_PROP||NO_KAB='3578' AND NO_KEC='$FROM_NO_KEC'");
	ociexecute($q);
	if (ocifetch($q)) { $DEST_NAMA_KEC = OCIResult($q, "NAMA_KEC"); }
	
	$q = ociparse($conn2, "SELECT * FROM SETUP_KEL WHERE NO_PROP||NO_KAB='3578' AND NO_KEC='$FROM_NO_KEC' AND NO_KEL='$FROM_NO_KEL'");
	ociexecute($q);
	if (ocifetch($q)) { $DEST_NAMA_KEL = OCIResult($q, "NAMA_KEL"); }
	

	/*$q= ociparse($conn2, "select * from REF_SIAK_WNI where sect='105' and NO='$ALASAN_PINDAH'");
	ociexecute($q);
	if (ocifetch($q)) { $ALASAN_PINDAH = OCIResult($q, "DESCRIP"); }*/

	$q = ociparse($conn2, "SELECT * FROM SETUP_PROP WHERE NO_PROP='$DEST_NO_PROP'");
	ociexecute($q);
	if (ocifetch($q)) { $FROM_NAMA_PROP = OCIResult($q, "NAMA_PROP"); }

	$q = ociparse($conn2, "SELECT * FROM SETUP_KAB WHERE NO_PROP='$DEST_NO_PROP' AND NO_KAB='$DEST_NO_KAB'");
	ociexecute($q);
	if (ocifetch($q)) { $FROM_NAMA_KAB = OCIResult($q, "NAMA_KAB"); }

	$q = ociparse($conn2, "SELECT * FROM SETUP_KEC WHERE NO_PROP='$DEST_NO_PROP' AND NO_KAB='$DEST_NO_KAB' AND NO_KEC='$DEST_NO_KEC'");
	ociexecute($q);
	if (ocifetch($q)) { $FROM_NAMA_KEC = OCIResult($q, "NAMA_KEC"); }
	
	$q = ociparse($conn2, "SELECT * FROM SETUP_KEL WHERE NO_PROP='$DEST_NO_PROP' AND NO_KAB='$DEST_NO_KAB' AND NO_KEC='$DEST_NO_KEC' AND NO_KEL='$DEST_NO_KEL'");
	ociexecute($q);
	if (ocifetch($q)) { $FROM_NAMA_KEL = OCIResult($q, "NAMA_KEL"); }
	}
	

$pdf->Cell(40,0,'DESA/KELURAHAN', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_KEL, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(40,0,'KECAMATAN', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_KEC, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(40,0,'KABUPATEN/KOTA', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_PROP, 0, 0, 'L', false);	

$pdf->Ln(10);
$pdf->SetFont('Times','UB',16);
$pdf->Cell(0,0,'FORMULIR PERMOHONAN PINDAH DATANG', 0, 0, 'C', false);	

$pdf->Ln(10);
$pdf->SetFont('Times','IB',14);
$pdf->SetFillColor(236,232,212);
$pdf->Cell(0,5,'*) Data Daerah Asal', 0, 0, 'L', true);	
$pdf->Ln(10);

$pdf->SetFont('Times','',12);
$pdf->Cell(5,0,'1.', 0, 0, 'C', false);	
$pdf->Cell(70,0,'Nomor Register', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NO_FORM, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(5,0,'2.', 0, 0, 'C', false);	
$pdf->Cell(70,0,'NIK Pemohon', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NIK_PEMOHON, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(5,0,'3.', 0, 0, 'C', false);	
$pdf->Cell(70,0,'Nama Lengkap', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NAMA_PEMOHON, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(5,0,'4.', 0, 0, 'C', false);	
$pdf->Cell(70,0,'Nomor Kartu Keluarga', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0, $NO_KK, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(5,0,'5.', 0, 0, 'C', false);	
$pdf->Cell(70,0,'Nama Kepala Keluarga', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$NAMA_KEP_KEL, 0, 0, 'L', false);	
$pdf->Ln(5);

$L_ALAMAT=strlen(trim($ALAMAT));

if($L_ALAMAT<45) {
	$pdf->Cell(5,0,'6.', 0, 0, 'C', false);	
	$pdf->Cell(70,0,'Alamat', 0, 0, 'L', false);	
	$pdf->Cell(5,0,':', 0, 0, 'C', false);	
	$pdf->Cell(0,0,$ALAMAT, 0, 0, 'L', false);	
	$pdf->Ln(5);
}else{
	$pdf->Ln(-2);
	$pdf->Cell(80,0,'', 0, 0, 'C', false);	
	$pdf->MultiCell(100,5,$ALAMAT,0,'L',false);
	$pdf->Ln(5);

	$pdf->Cell(5,-25,'6.', 0, 0, 'C', false);	
	$pdf->Cell(70,-25,'Alamat', 0, 0, 'L', false);	
	$pdf->Cell(5,-25,':', 0, 0, 'C', false);	
	$pdf->Ln(-2);
}

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'a. Rt/Rw', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$RT.'/'.$RW, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'b. Kode Pos', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$KODE_POS, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'c. Telp', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$TELP, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'d. Kelurahan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_KEL, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'e. Kecamatan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_KEC, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'f. Kabupaten', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_KAB, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'g. Propinsi', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$FROM_NAMA_PROP, 0, 0, 'L', false);	

$pdf->Ln(7);
$pdf->SetFont('Times','IB',14);
$pdf->SetFillColor(236,232,212);
$pdf->Cell(0,5,'*) Data Kedatangan', 0, 0, 'L', true);	
$pdf->Ln(10);

$pdf->SetFont('Times','',12);
$L_DEST_ALAMAT=strlen(trim($DEST_ALAMAT));

if($L_DEST_ALAMAT<45) {
	$pdf->Cell(5,0,'1.', 0, 0, 'C', false);	
	$pdf->Cell(70,0,'Alamat Tujuan Datang', 0, 0, 'L', false);	
	$pdf->Cell(5,0,':', 0, 0, 'C', false);	
	$pdf->Cell(0,0,$DEST_ALAMAT, 0, 0, 'L', false);	
	$pdf->Ln(5);
}else{
	$pdf->Ln(-2);
	$pdf->Cell(80,0,'', 0, 0, 'C', false);	
	$pdf->MultiCell(100,5,$DEST_ALAMAT,0,'L',false);
	$pdf->Ln(5);
	$pdf->Cell(5,-25,'1.', 0, 0, 'C', false);	
	$pdf->Cell(70,-25,'Alamat Tujuan Datang', 0, 0, 'L', false);	
	$pdf->Cell(5,-25,':', 0, 0, 'C', false);	
	$pdf->Ln(-2);
}

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'a. Rt/Rw', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$DEST_NO_RT.'/'.$DEST_NO_RW, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'b. Kode Pos', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$DEST_KODE_POS, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'c. Telp', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$DEST_TELP, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'d. Kelurahan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$DEST_NAMA_KEL, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'e. Kecamatan', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$DEST_NAMA_KEC, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'f. Kabupaten', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$DEST_NAMA_KAB, 0, 0, 'L', false);	
$pdf->Ln(5);

$pdf->Cell(10,0,'', 0, 0, 'C', false);	
$pdf->Cell(65,0,'g. Propinsi', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$DEST_NAMA_PROP, 0, 0, 'L', false);	
$pdf->Ln(5);


$pdf->Cell(5,0,'2.', 0, 0, 'C', false);	
$pdf->Cell(70,0,'Status KK Bagi Yang Datang', 0, 0, 'L', false);	
$pdf->Cell(5,0,':', 0, 0, 'C', false);	
$pdf->Cell(0,0,$arrStPindah[$STATUS_YG_PINDAH], 0, 0, 'L', false);	
$pdf->Ln(10);

$pdf->SetFont('Times','IB',14);
$pdf->SetFillColor(236,232,212);
$pdf->Cell(0,5,'*) Keluarga Yang Datang ', 0, 0, 'L', true);	
$pdf->Ln(7);

$pdf->SetFont('Times','',10);

$header = array(
	array("label"=>"NO", "length"=>10, "align"=>"C"),
	array("label"=>"NIK", "length"=>30, "align"=>"L"),
	array("label"=>"NAMA LENGKAP", "length"=>75, "align"=>"L"),
	//array("label"=>"TGL LHR", "length"=>40, "align"=>"L"),
	array("label"=>"TGL KTP", "length"=>35, "align"=>"L"),
	array("label"=>"SHDK", "length"=>40, "align"=>"L"),
);

$pdf->SetFillColor(236,232,212);
foreach ($header as $kolom) {
	$pdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
}	
$pdf->Ln(5);

$sql_dtl = ociparse($conn, "select a.*
        from DATANG_DETAIL a where ID_DATANG = ".$id);
ociexecute($sql_dtl);
while(ocifetch($sql_dtl)){$i++;
	$pdf->Cell(10,5,$i, 1, '0', 'C', false);	
	$pdf->Cell(30,5,OCIResult($sql_dtl,'NIK'), 1, '0', 'L', false);		
	$pdf->Cell(75,5,OCIResult($sql_dtl,'NAMA_LENGKAP'), 1, '0', 'L', false);		
	$pdf->Cell(35,5,OCIResult($sql_dtl,'TGL_KTP'), 1, '0', 'L', false);		
	$pdf->Cell(40,5,$arrSHDK[OCIResult($sql_dtl,'SHDK')], 1, '0', 'L', false);		
	$pdf->Ln(5);

}

$pdf->Output();
?> 
