<?php
error_reporting(0);

require_once("check_session.php");
require_once("helpers.php");
require_once("header.php");
$arrStPindah = array('','Numpang KK','Membuat KK Baru','Nomor KK Tetap');
$arrKla = array('','Pindah Antar Kelurahan','Pindah Antar Kecamatan','Pindah Antar Kabupaten','Pindah Antar Propinsi','Pindah Keluar Negeri');
$arrSHDK = array('KEPALA KELUARGA','SUAMI','ISTRI','ANAK','MENANTU','CUCU','ORANG TUA','MERTUA','FAMILI LAIN','PEMBANTU','LAINNYA');

$source_path = explode('/',$_SERVER['REQUEST_URI']);
$upload_dir = $_SERVER['DOCUMENT_ROOT'].'/'.$source_path[1].'/upload/';
?>

<?php
    if(isset($_GET['id'])) {$ID=(get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);}
 
    if(!empty($ID)) {
        $sql = ociparse($conn, "select a.*
        from DATANG_HEADER a where ID = ".$ID);
        ociexecute($sql);
        
        while ($row = oci_fetch_array ($sql, OCI_ASSOC)) {
            $a_row = $row;        
        }
    }
 
	if(isset($_POST['Simpan'])) {       
        // Data Daerah Asal		
        $record['NO_DATANG']    = genNoDatang($conn, date('dmY'));
        $record['NO_FORM']      = genNoForm($conn, date('dmY'));
		$record['NO_PINDAH']    = $_POST['txtNoskp'];
		$record['NO_KK']        = $_POST['txtNkk'];
		$record['NAMA_KEP']     = $_POST['txtNamaKep'];
		$record['NIK_PEMOHON']  = $_POST['txtNik'];
		$record['NAMA_PEMOHON'] = $_POST['txtNamaPemohon'];
		$record['SRC_DUSUN']    = $_POST['txtAlamat'];
		$record['SRC_RT']       = $_POST['txtRT'];
		$record['SRC_RW']       = $_POST['txtRW'];
		$record['SRC_PROP']     = $_POST['mnuProp'];
		$record['SRC_KAB']      = $_POST['txtIdKab'];
        $record['SRC_KEC']      = $_POST['txtIdKec'];
        $record['SRC_KEL']      = $_POST['txtIdKel'];
        $record['SRC_KODE_POS'] = $_POST['txtKdpos'];
        $record['SRC_TELP']     = $_POST['txtTelp'];
        
        // Data Daerah Tujuan
        $record['NK_NO_KK']         = $_POST['txtNkNkk']; 
        $record['NK_NIK_KEP_KEL']   = $_POST['txtNkNik']; 
        $record['NK_NAMA_KEP_KEL']  = $_POST['txtNKNamaKep']; 
        $record['TGL_DATANG']       = "{TO_DATE('$_POST[tanggal]', 'dd/mm/yyyy')}"; 
        $record['NK_ALAMAT']        = $_POST['txtDestAlamat']; 
        $record['NK_RT']            = $_POST['txtDestRT']; 
        $record['NK_RW']            = $_POST['txtDestRW']; 
        $record['NK_KODE_POS']      = $_POST['txtDestKdpos']; 
        $record['NK_TELP']          = $_POST['txtDestTelp']; 
        $record['STATUS_PINDAH']    = $_POST['mnuStatusKKPindah']; 
        $record['NK_PROP']          = $_POST['mnuPropDest'];
        $record['NK_KAB']           = $_POST['txtDestIdKab'];
        $record['NK_KEC']           = $_POST['txtDestIdKec'];
        $record['NK_KEL']           = $_POST['txtDestIdKel'];
        $record['NK_KODE_POS']      = $_POST['txtDestKdpos'];
        $record['NK_TELP']          = $_POST['txtDestTelp'];
		
		//Keluarga 
        if(isset($_FILES)) {
            foreach($_FILES as $key => $value) {
                // Dokumen Persyatan
                if(!empty($value['name'])) {
                    $record[strtoupper($key)] = uploadFile($upload_dir, $_FILES[$key], $record['NIK_PEMOHON'], $key);
                }
            }
        }
        
        if(empty($ID)) {
            $id = insertRecord($conn, "DATANG_HEADER", $record);
        
            if($id)
                setFlash(thisPage(), array(1, "Data berhasil disimpan"));
            else
                setFlash(thisPage(), array(0, "Data gagal disimpan"));
        } else {
            $id = updateRecord($conn, "DATANG_HEADER", $record, $ID);
            
            setFlash(thisPage(), array(1, "Data berhasil diedit"));
        }
        
        if($id and isset($_POST['txtNikDtl'])) {
            foreach($_POST['txtNikDtl'] as $key => $value) {
                $recorddetail['NO_DATANG'] = $record['NO_DATANG'];
                $recorddetail['ID_DATANG'] = $id;
                $recorddetail['NIK'] = $_POST['txtNikDtl'][$key];
                $recorddetail['NAMA_LENGKAP'] = $_POST['txtNamaDtl'][$key];
                $recorddetail['SHDK'] = $_POST['mnuSHDK'][$key];

                if($key == 0)
                    insertRecord($conn, "DATANG_DETAIL", $recorddetail);
                elseif($_POST['isdelete'][$key] == 1 )
                    deleteRecord($conn, "DATANG_DETAIL", "ID_DETAIL" , $key);
                else
                    updateRecord($conn, "DATANG_DETAIL", $recorddetail, $key);    
            }    
        }
        
        if($id) {
            header( 'Location: datang.php' ) ;
        }
	}
?>

<?php
// Combo SHDK
$sqlSHDK = ociparse($conn2, "select * from HUBKELUARGA_MASTER order by no ");
ociexecute($sqlSHDK);
$cmbSHDK="<select name='mnuSHDK' id='mnuSHDK'>";
$cmbSHDK.="<option value=''>-- Pilih--</option>";
/*while (ocifetch($sqlSHDK)) {
    $cmbSHDK.="<option value='".OCIResult($sqlSHDK, 'NO')."'";
    if(OCIResult($sqlSHDK, 'NO')==$SHDK) $cmbSHDK.=" selected";
    $cmbSHDK.=">".OCIResult($sqlSHDK, 'DESCRIP')."</option>"; 
}*/
foreach($arrSHDK as $key => $value) {
    $no = ++$key;
    $cmbSHDK.= "<option value='$no'>$value</option>";
}
$cmbSHDK.="</select>";

// Combo Jenis Kelamin
$sqlSex = ociparse($conn2, "select * from REF_SIAK_WNI where sect='801' order by NO");
ociexecute($sqlSex);
$cmbSex="<select name='mnuSex' id='mnuSex'>";
$cmbSex.="<option value=''>-- Pilih--</option>";
while (ocifetch($sqlSex)) {
    $cmbSex.="<option value='".OCIResult($sqlSex, 'NO')."'";
    if(OCIResult($sqlSex, 'NO')==$JENIS_KLMIN) $cmbSex.=" selected";
    $cmbSex.=">".OCIResult($sqlSex, 'DESCRIP')."</option>"; 
}
$cmbSex.="</select>";

// Combo Jenis Kelamin
$sqlKawin = ociparse($conn2, "select * from REF_SIAK_WNI where sect='601' order by NO");
ociexecute($sqlKawin);
$cmbKawin="<select name='mnuKawin' id='mnuKawin' >";
$cmbKawin.="<option value=''>-- Pilih Status Kawin --</option>";
while (ocifetch($sqlKawin)) {
    $cmbKawin.="<option value='".OCIResult($sqlKawin, 'NO')."'";
    if(OCIResult($sqlKawin, 'NO')==$STAT_KWN) $cmbKawin.=" selected";
    $cmbKawin.=">".OCIResult($sqlKawin, 'DESCRIP')."</option>"; 
}
$cmbKawin.="</select>";

// Combo Jenis Kepindahan
$sqlJenis = ociparse($conn2, "select * from REF_SIAK_WNI where sect='107' order by NO");
ociexecute($sqlJenis);
$cmbJenis="<select name='mnuJenis' id='mnuJenis'>";
$cmbJenis.="<option value=''>-- Pilih--</option>";
while (ocifetch($sqlJenis)) {
    $cmbJenis.="<option value='".OCIResult($sqlJenis, 'NO')."'";
    if(OCIResult($sqlJenis, 'NO')==$a_row['STATUS_PINDAH']) $cmbJenis.=" selected";
    $cmbJenis.=">".OCIResult($sqlJenis, 'DESCRIP')."</option>"; 
}
$cmbJenis.="</select>";
?>

<style>
select, option { width:230px;}
.error { color:#FF0000;}
table { margin:0;}
.show {
	/*display:block;*/
	visibility:visible;
}
.hide {
	display:none;
	visibility:hidden;
}
.titlefrm {
    background: none repeat scroll 0 0 #000;
    color: #fff;
    padding: 3px 5px;
    width: auto !important;
	margin:0;
}	
</style>
<!--
<script type="text/javascript" src="validasi.js"></script>
-->
<!-- styles -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/jquery.gritter.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" href="css/font-awesome.css">
<link href="css/tablecloth.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="dropdown.js"></script>
<link rel="stylesheet" href="assets/css/jquery-ui.css" />
<link rel="stylesheet" href="assets/css/jquery-ui-timepicker-addon.css" />
<script type="text/javascript" src="assets/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery/jquery-ui.js"></script>


<div class="container">
<div id="post" class="post">

<?php 
$i_message = getFlash(thisPage());
if($i_message) {
?>
<div class="alert <?= ($i_message['status']) ? "alert-success" : "alert-danger"?>">
  <strong><?= $i_message['message']?></strong> 
</div>
<?php }?>

<h1>Form Permohonan Pindah Datang WNI</h1>
<form name="form" method="POST" action="" onSubmit="" enctype="multipart/form-data" >

<div class="titlefrm"><i>** Data Daerah Asal </i></div>
<table width="100%">
	<?php if(!empty($ID)){?>
    <tr>
      <td width="30%" align="right" class="tablecontent" required="required">ID &nbsp;</td>
      <td width="70%"> 
        <b><?php echo str_pad($a_row['ID'], 4, 0, STR_PAD_LEFT)?></b> 
      </td>
    </tr>
    <tr>
      <td width="30%" align="right" class="tablecontent" required="required">Nomor Form &nbsp;</td>
      <td width="70%"> 
        <b><?php echo $a_row['NO_FORM']?></b> 
      </td>
    </tr>
    <?php }?>
    <tr>
	  <td width="30%" align="right" class="tablecontent" required="required">Nomor Surat Keterangan Pindah &nbsp;</td>
	  <td width="70%"><input name="txtNoskp" type="text" id="txtNoskp" size="25" maxlength="25" value="<?= $a_row['NO_DATANG']?>" required="required"> 
    </tr>
	<tr>
	  <td width="30%" align="right" class="tablecontent" required="required">NIK Pemohon &nbsp;</td>
	  <td width="70%"><input name="txtNik" type="text" id="txtNik" size="20" maxlength="16" value="<?= $a_row['NIK_PEMOHON']?>" required="required"></td>
    </tr>
</table>


<table width="100%" id="getDataHeader">
	<tr>
	  <td width="30%" align="right" class="tablecontent">Nama Lengkap &nbsp;</td>
	  <td  width="70%"><input name="txtNamaPemohon" type="text" id="txtNamaPemohon" size="50" maxlength="60" value="<?= $a_row['NAMA_PEMOHON']?>" required="required">
	  <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Nomor Kartu Keluarga &nbsp;</td>
	  <td><input name="txtNkk" type="text" id="txtNkk" size="20" maxlength="16" value="<?= $a_row['NO_KK']?>" required="required">
	  <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Nama Kepala Keluarga &nbsp;</td>
	  <td ><input name="txtNamaKep" type="text" id="txtNamaKep" size="50" maxlength="60" value="<?= $a_row['NAMA_KEP']?>">
	  <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Alamat &nbsp;</td>
	  <td><input name="txtAlamat" type="text" id="txtAlamat" size="50" maxlength="100" value="<?= $a_row['SRC_ALAMAT']?>"> <span class="error">&nbsp;*</span></td>
	</tr>
	<tr>
	  <td align="right" class="tablecontent">RT / RW &nbsp;</td>
	  <td><input name="txtRT" type="text" id="txtRT" size="10" maxlength="3" value="<?= $a_row['SRC_RT']?>" > / <input name="txtRW" type="text" id="txtRW" size="10" maxlength="3" value="<?= $a_row['SRC_RW']?>" > <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Propinsi &nbsp;</td>
	  <td><select name="mnuProp" id="mnuProp" >
<option value="">--Pilih Provinsi--</option>
<?php
$propinsi = ociparse($conn2, "select * from SETUP_PROP order by NAMA_PROP");
ociexecute($propinsi);
while (ocifetch($propinsi)) { 
	if(OCIResult($propinsi, 'NO_PROP')==$a_row['SRC_PROP']){ $sl = ' selected'; } else {$sl = '';}
	echo '<option value="'.OCIResult($propinsi, 'NO_PROP').'"'.$sl.'>'.OCIResult($propinsi, 'NAMA_PROP').'</option>\n';
}
?>
</select> <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Kabupaten &nbsp;</td>
	  <td><select name="txtIdKab" id="txtIdKab" >
<option value="">--Pilih Kabupaten/Kota--</option>
<?php
$kota = ociparse($conn2, "SELECT * FROM SETUP_KAB WHERE NO_PROP='$SRC_PROP' order by nama_kab");
ociexecute($kota);
while (ocifetch($kota)) {
	if(OCIResult($kota, 'NO_KAB')==$a_row['SRC_KAB']){ $sl = ' selected'; } else {$sl = '';}
	echo '<option value="'.OCIResult($kota, 'NO_KAB').'"'.$sl.'>'.OCIResult($kota, 'NAMA_KAB').'</option>\n';
}
?>
</select> <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Kecamatan &nbsp;</td>
	  <td><select name="txtIdKec" id="txtIdKec" >
<option value="">--Pilih Kecamatan--</option>
<?php
$kec = ociparse($conn2, "SELECT * FROM SETUP_KEC WHERE NO_PROP='$SRC_PROP' AND NO_KAB='$SRC_KAB' order by nama_kec");
ociexecute($kec);
while (ocifetch($kec)) {
	if(OCIResult($kec, 'NO_KEC')==$a_row['SRC_KEC']){ $sl = ' selected'; } else {$sl = '';}
	echo '<option value="'.OCIResult($kec, 'NO_KEC').'"'.$sl.'>'.OCIResult($kec, 'NAMA_KEC').'</option>\n';
}

?>
</select> <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Kelurahan &nbsp;</td>
	  <td><select name="txtIdKel" id="txtIdKel" >
<option value="">--Pilih Kelurahan--</option>
<?php
$kel = ociparse($conn2, "SELECT * FROM SETUP_KEL where NO_PROP='$SRC_PROP' AND NO_KAB='$SRC_KAB' AND NO_KEC='$SRC_KEC' order by nama_kel");
ociexecute($kel);
while (ocifetch($kel)) {
	if(OCIResult($kel, 'NO_KEL')==$a_row['SRC_KEL']){ $sl = ' selected'; } else {$sl = '';}
	echo '<option value="'.OCIResult($kel, 'NO_KEL').'"'.$sl.'>'.OCIResult($kel, 'NAMA_KEL').'</option>\n';
}

?>
</select> <span class="error">&nbsp;*</span></td>
	</tr>
    
	<tr>
	  <td align="right" class="tablecontent">Kode Pos &nbsp;</td>
	  <td><input name="txtKdpos" type="text" id="txtKdpos" size="10" maxlength="5" value="<?= $a_row['SRC_KODE_POS']?>"></td>
	</tr>
	<tr>
	  <td align="right" class="tablecontent">Telp &nbsp;</td>
	  <td><input name="txtTelp" type="text" id="txtTelp" size="20" maxlength="30" value="<?= $a_row['SRC_TELP']?>"></td>
	</tr>

</table>

<div class="titlefrm"><i>** Data Daerah Tujuan</i></div>
<table width="100%">
	<tr>
	  <td align="right" class="tablecontent">Nomor Kartu Keluarga &nbsp;</td>
	  <td><input name="txtNkNkk" type="text" id="txtNkNkk" size="20" maxlength="16" value="<?= $a_row['NK_NO_KK']?>" required="required">
	  <span class="error">&nbsp;*</span></td>
	</tr>

<tr>
	  <td width="30%" align="right" class="tablecontent" required="required">NIK Kepala Keluarga &nbsp;</td>
	  <td width="70%"><input name="txtNkNik" type="text" id="txtNkNik" size="20" maxlength="16" value="<?= $a_row['NK_NIK_KEP_KEL']?>" required="required"> 
		  <span class="error">&nbsp;*</span></td>
      <input name="txtID" type="hidden" id="txtID" value=""><input name="txtNoForm" type="hidden" id="txtNoForm" value=""></td>
    </tr>

	<tr>
	  <td align="right" class="tablecontent">Nama Kepala Keluarga &nbsp;</td>
	  <td ><input name="txtNKNamaKep" type="text" id="txtNKNamaKep" size="50" maxlength="60" value="<?= $a_row['NK_NAMA_KEP_KEL']?>">
	  <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Tanggal Kedatangan &nbsp;</td>
    <td><input type="text" name="tanggal" id="tanggal" value="<?= $a_row['TGL_DATANG']?>" /></td>
	  
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Alamat Tujuan Pindah &nbsp;</td>
	  <td><input name="txtDestAlamat" type="text" id="txtDestAlamat" size="50" maxlength="100" value="<?= $a_row['NK_ALAMAT']?>"> <span class="error">&nbsp;*</span></td>
	</tr>
	<tr>
	  <td align="right" class="tablecontent">RT / RW &nbsp;</td>
	  <td><input name="txtDestRT" type="text" id="txtDestRT" size="10" maxlength="3" value="<?= $a_row['NK_RT']?>">  / <input name="txtDestRW" type="text" id="txtDestRW" size="10" maxlength="3" value="<?= $a_row['NK_RW']?>" > <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Propinsi &nbsp;</td>
	  <td><select name="mnuPropDest" id="mnuPropDest" >
<option value="">--Pilih Provinsi--</option>
<?php
$propinsi = ociparse($conn2, "select * from SETUP_PROP order by NAMA_PROP");
ociexecute($propinsi);
while (ocifetch($propinsi)) { 
	if(OCIResult($propinsi, 'NO_PROP')==$a_row['NK_PROP']){ $sl = ' selected'; } else {$sl = '';}
	echo '<option value="'.OCIResult($propinsi, 'NO_PROP').'"'.$sl.'>'.OCIResult($propinsi, 'NAMA_PROP').'</option>\n';
}
?>
</select> <span class="error">&nbsp;*</span></td>
	</tr>


	<tr>
	  <td align="right" class="tablecontent">Kabupaten &nbsp;</td>
	  <td><select name="txtDestIdKab" id="txtDestIdKab" >
<option value="">--Pilih Kabupaten/Kota--</option>
<?php
$kota = ociparse($conn2, "SELECT * FROM SETUP_KAB WHERE NO_PROP='$NK_PROP' order by nama_kab");
ociexecute($kota);
while (ocifetch($kota)) {
	if(OCIResult($kota, 'NO_KAB')==$a_row['NK_KAB']){ $sl = ' selected'; } else {$sl = '';}
	echo '<option value="'.OCIResult($kota, 'NO_KAB').'"'.$sl.'>'.OCIResult($kota, 'NAMA_KAB').'</option>\n';
}
?>
</select> <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Kecamatan &nbsp;</td>
	  <td><select name="txtDestIdKec" id="txtDestIdKec" >
<option value="">--Pilih Kecamatan--</option>
<?php
$kec = ociparse($conn2, "SELECT * FROM SETUP_KEC WHERE NO_PROP='$NK_PROP' AND NO_KAB='$NK_KAB' order by nama_kec");
ociexecute($kec);
while (ocifetch($kec)) {
	if(OCIResult($kec, 'NO_KEC')==$a_row['NK_KEC']){ $sl = ' selected'; } else {$sl = '';}
	echo '<option value="'.OCIResult($kec, 'NO_KEC').'"'.$sl.'>'.OCIResult($kec, 'NAMA_KEC').'</option>\n';
}

?>
</select> <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Kelurahan &nbsp;</td>
	  <td><select name="txtDestIdKel" id="txtDestIdKel" >
<option value="">--Pilih Kelurahan--</option>
<?php
$kel = ociparse($conn2, "SELECT * FROM SETUP_KEL where NO_PROP='$DEST_NO_PROP' AND NO_KAB='$DEST_NO_KAB' AND NO_KEC='$DEST_NO_KEC' order by nama_kel");
ociexecute($kel);
while (ocifetch($kel)) {
	if(OCIResult($kel, 'NO_KEL')==$a_row['NK_KEL']){ $sl = ' selected'; } else {$sl = '';}
	echo '<option value="'.OCIResult($kel, 'NO_KEL').'"'.$sl.'>'.OCIResult($kel, 'NAMA_KEL').'</option>\n';
}

?>
</select> <span class="error">&nbsp;*</span></td>
	</tr>
	<tr>
	  <td align="right" class="tablecontent">Kode Pos &nbsp;</td>
	  <td><input name="txtDestKdpos" type="text" id="txtDestKdpos" size="10" maxlength="5" value="<?php echo $a_row['NK_KODE_POS'];?>"></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Telp &nbsp;</td>
	  <td><input name="txtDestTelp" type="text" id="txtDestTelp" size="20" maxlength="30" value="<?php echo $a_row['NK_TELP'];?>"></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Jenis Kepindahan &nbsp;</td>
	  <td><?php echo $cmbJenis; ?></td>
	</tr>


	<tr>
	  <td align="right" class="tablecontent">Status KK Bagi Yang Pindah &nbsp;</td>
	  <td><select name="mnuStatusKKPindah" class="inputField" id="mnuStatusKKPindah">
		<option value=''>-- Pilih--</option>
		<?php for($i=1,$n=3;$i<=$n;$i++){ ?>
        <option class="inputField" value="<?=$i;?>" <?php if($STATUS_YG_PINDAH==$i) echo "selected";?>><?php echo $arrStPindah[$i] ;?></option>
        <?php } ?>
    </select></td>
	</tr>

</table>
<div class="titlefrm"><i>** Dokumen Persyaratan </i></div>
<table width="100%">
	<tr>
	  <td width="30%" align="right" class="tablecontent">KTP <i style="color:red">(.jpg)</i>&nbsp;</td>	
	  <td width="70%"><input type="file" name="fl_ktp"><?php $fktpexist = 'upload/'.$a_row['FL_KTP'].'';
		if (is_file($fktpexist)) { ?> <img src="images/ya.png" onClick="pKTP('<?php echo $ID?>')" style="cursor:pointer" />
		<?php }?>
        </td> 
    </tr>
	<tr>
	  <td align="right" class="tablecontent">KK <i style="color:red">(.jpg)</i>&nbsp;</td>	
	  <td><input type="file" name="fl_kk"><?php $fkkexist = 'upload/'.$a_row['FL_KK'].'';
		if (is_file($fkkexist)) { ?> <img src="images/ya.png" onClick="pKK('<?php echo $ID?>')" style="cursor:pointer" />
		<?php }?>        </td>
    </tr>
	<tr>
	  <td width="30%" align="right" class="tablecontent">Surat Keterangan Pindah WNI <i style="color:red">(.jpg)</i>&nbsp;</td>	
	  <td width="70%"><input type="file" name="fl_skpindah"><?php $fskexist = 'upload/'.$a_row['FL_SKPINDAH'].'';
		if (is_file($fskexist)) { ?> <img src="images/ya.png" onClick="pSK('<?php echo $ID?>')" style="cursor:pointer" />
		<?php }?>
		 </td> 
    </tr>
	<tr>
	  <td width="30%" align="right" class="tablecontent">Akta Kawin <i style="color:red">(.jpg)</i>&nbsp;</td>	
	  <td width="70%"><input type="file" name="fl_aktakawin"><?php $faktaexist = 'upload/'.$a_row['FL_AKTAKAWIN'].'';
		if (is_file($faktaexist)) { ?> <img src="images/ya.png" onClick="pAkta('<?php echo $ID?>')" style="cursor:pointer" />
		<?php }?>
		 </td> 
    </tr>
		<tr>
	  <td width="30%" align="right" class="tablecontent">Surat Jaminan Tempat Tinggal <i style="color:red">(.jpg)</i>&nbsp;</td>	
	  <td width="70%"><input type="file" name="fl_jaminan"><?php $fjaminanexist = 'upload/'.$a_row['FL_JAMINAN'].'';
		if (is_file($fjaminanexist)) { ?> <img src="images/ya.png" onClick="pJaminan('<?php echo $ID?>')" style="cursor:pointer" />
		<?php }?>
		 </td> 
    </tr>
</table>
<div class="titlefrm"><i>** Keluarga Yang Datang </i></div>
<table width="100%">
	<tr>
	  <td width="30%" align="right" class="tablecontent">NIK &nbsp;</td>	
	  <td width="70%"><input name="txtNikDtl" type="text" id="txtNikDtl" size="20" maxlength="16"> 
	  <input name="txtIDDtl" type="hidden" id="txtIDDtl" value="">
</td>
    </tr>
</table>
<table width="100%" id="getDataDetail">

	<tr>
	  <td width="30%" align="right" class="tablecontent">Nama Lengkap &nbsp;</td>
	  <td width="70%" ><input name="txtNamaDtl" type="text" id="txtNamaDtl" size="50" maxlength="60"><input name="txtPortalDetail" type="hidden" id="txtPortalDetail" value="">
	  <span class="error">&nbsp;*</span></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">SHDK &nbsp;</td>
	  <td><?php echo $cmbSHDK; ?></td>
	</tr>


	<tr>
	  <td align="right" class="tablecontent">Jenis Kelamin &nbsp;</td>
	  <td><?php echo $cmbSex; ?></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Status Perkawinan &nbsp;</td>
	  <td><?php echo $cmbKawin; ?></td>
	</tr>

	<tr>
	  <td align="right" class="tablecontent">Tanggal Lahir &nbsp;</td>
	  <td><input name="txtTglLhr" type="text" id="txtTglLhr" size="10" maxlength="10" value="<?php echo $_POST['txtTglLhr'];?>"> (dd/mm/yyyy)</td>
	</tr>

</table>

<table width="100%">
	<tr class="button">
	  <td width="30%" align="right" >&nbsp;</td>
	  <td width="70%">
      <input name="SimpanDtl" value="Tambah" type="button"  onclick="return pValidasiDtl()"/>
      </td>
	</tr>

</table>

<table width="100%" id="subtable" align="center">
	<tr class="subheader">
	    <td width="3%">NO</td>
	    <td width="18%">NIK</td>
	    <td width="35%">NAMA LENGKAP</td>
	    <td width="18%">MASA BERLAKU KTP</td>
	    <td width="20%">SHDK</td>
	    <td width="10%">&nbsp;</td>
    </tr>
<?php
 if(!empty($ID)) {
        $sql_dtl = ociparse($conn, "select a.*
        from DATANG_DETAIL a where ID_DATANG = ".$ID);
        ociexecute($sql_dtl);
	$n = 0;
    while ($row = oci_fetch_array ($sql_dtl, OCI_ASSOC)) {
        $a_drow = $row;        
        
		$class = ($n%2==0) ? 'tablecontent' : 'tablecontent-odd'; ?>
        <tr class="<?php echo $class; ?>">
            <td align="center"><?php echo ++$n; ?>
                <input type="hidden" id="txtNikDtl[<?= $a_drow['ID_DETAIL']?>]" name="txtNikDtl[<?= $a_drow['ID_DETAIL']?>]" value='<?= $a_drow['NIK']?>' />
                <input type="hidden" id="txtNamaDtl[<?= $a_drow['ID_DETAIL']?>]" name="txtNamaDtl[<?= $a_drow['ID_DETAIL']?>]" value='<?= $a_drow['NAMA_LENGKAP']?>' />
                <input type="hidden" id="mnuSHDK[<?= $a_drow['ID_DETAIL']?>]" name="mnuSHDK[<?= $a_drow['ID_DETAIL']?>]" value='<?= $a_drow['SHDK']?>' />    
                <input type="hidden" id="isdelete[<?= $a_drow['ID_DETAIL']?>]" name="isdelete[<?= $a_drow['ID_DETAIL']?>]" value='0' />    
            </td>
            <td><?php echo $a_drow['NIK']; ?></td>
            <td><?php echo $a_drow['NAMA_LENGKAP']; ?></td>
            <td><?php echo $a_drow['TGL_KTP']; ?></td>
            <td><?php echo $arrSHDK[$a_drow['SHDK']]; ?></td>
            <td align="center"><img src="images/b_drop.png" title="hapus" class="img-button" onClick="removeDetail(this, 1)" style="cursor:pointer"></td>
        </tr> 
		<?php	
    }
}
?>
</table>

<table width="100%">
	<tr class="button">
	  <td width="100%" style="padding:5px 0;" >
      <input name="Simpan" value="Simpan" type="submit" style="width:30%; height:40px; font-size:20px; background:#333333; color:#fff;" />
      </td>
	</tr>

</table>

</form>
	</div><!-- end of #post-16 -->
</div><!-- end of #content -->
<?php
require_once("footer.php");
?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#tanggal").datepicker({ dateFormat: 'dd/mm/yy' }).datepicker("setDate", new Date());
        $("#txtTglLhr").datepicker({ 
            dateFormat: 'dd/mm/yy', 
            changeMonth: true,
            changeYear: true,
            yearRange: "-80:+0",
        });
    });
    
    var idx = <?= $n?>;
    
    function pValidasiDtl() {
        if ((document.getElementById('txtNikDtl').value) == "") {
            alert ("NIK harus diisi");
            document.getElementById('txtNikDtl').focus();
            return false;
        }

        if ((document.getElementById('txtNamaDtl').value) == "") {
            alert ("Nama lengkap harus diisi");
            document.getElementById('txtNamaDtl').focus();
            return false;
        }
        
        var table = $('table#subtable');
        var optSHDK = ["<?php echo implode('","',$arrSHDK)?>"];
        var html =
            '<tr>'+
            '<td width="3%">'+ (++idx) +
            '<input type="hidden" id="txtNikDtl[0]" name="txtNikDtl[0]" value='+ $('#txtNikDtl').val() +' />' +
            '<input type="hidden" id="txtNamaDtl[0]" name="txtNamaDtl[0]" value='+ $('#txtNamaDtl').val() +' />' +
            '<input type="hidden" id="mnuSHDK[0]" name="mnuSHDK[0]" value='+ $('#mnuSHDK').val() +' />' +
            '<input type="hidden" id="isdelete[0]" name="isdelete[0]" value="0" />' +
            '</td>'+
            '<td width="18%">'+ $('#txtNikDtl').val() +'</td>'+
            '<td width="35%">'+ $('#txtNamaDtl').val() +'</td>'+
            '<td width="18%" ></td>'+
            '<td width="20%">'+ optSHDK[(new Number($('#mnuSHDK').val()) - 1)] +'</td>'+
            '<td width="10%"><img src="images/b_drop.png" style="cursor:pointer" onclick="removeDetail(this, 0)" /></td>'+
            '</tr>';
            
        table.append(html);
    }
    
    function removeDetail(obj, isdelete) {
        if(isdelete) {
            $(obj).parents('tr').find('input[id^="isdelete"]').val(1);
            $(obj).parents('tr').hide();
            
            console.log($(obj).parents('tr').children('input[id^="isdelete"]'));    
        } else
            $(obj).parents('tr').remove();
    }
    
    function pKTP(id) {
        var _url ='scan_ktp.php?id='+id;
        window.open(_url,'','width=600,height=400,left=250,top=50,scrollbars=yes,menubar=no,resize=no');
    }

    function pKK(id) {
        var _url ='scan_kk.php?id='+id;
        window.open(_url,'','width=800,height=400,left=250,top=50,scrollbars=yes,menubar=no,resize=no');
    }
    
    function pSK(id) {
        var _url ='scan_sk.php?id='+id;
        window.open(_url,'','width=800,height=400,left=250,top=50,scrollbars=yes,menubar=no,resize=no');
    }
    
    function pAkta(id) {
        var _url ='scan_akta.php?id='+id;
        window.open(_url,'','width=800,height=400,left=250,top=50,scrollbars=yes,menubar=no,resize=no');
    }
    
    function pJaminan(id) {
        var _url ='scan_jaminan.php?id='+id;
        window.open(_url,'','width=800,height=400,left=250,top=50,scrollbars=yes,menubar=no,resize=no');
    }
    
</script>
<style>
th, td, table { border:none; padding:1px 0;}
h4 { padding:0;}
option { padding:4px 8px; }
.btn-success{
    background-color: #95b75d;
    border-color: #95b75d;
    color: #ffffff;
	cursor:pointer;
	vertical-align:middle;
}
.tablecontent {
	font-weight: lighter;
	background-color:#F3F2F2;
	border-bottom:1px solid #fff;
}
.tablecontent-odd {
}
.btn { cursor:pointer;}
.subheader {background:#333333; color:#fff; font-weight:bold; }
.subheader td { border:1px solid #dddddd;  padding: 0.8em;}
.tablecontent td { }
</style>



